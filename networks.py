import torch
from torch import nn
from vit import ViT

def get_small_vit(args: dict, heads: int = 12) -> ViT:
    model = ViT(
        args,
        head=heads,
    )
    if not args["model"]["bias"]:
        for name, layer in model.named_modules():
            if isinstance(layer, nn.Linear):
                layer.bias = None
    
    return model



# DEBUGGING ----------------------------------------------------------------------

def main():
    pass

if __name__ == "__main__":
    main()