from torchvision import models
import torch
from torch import nn
from typing import List, Union, Tuple, Optional, Dict
from networks import get_small_vit
from otfusion_sidak import wasserstein_ensemble, parameters
from heads_alignment import align_heads, get_specific_head_permutation
from in_head_alignment import in_head_alignment
from train import load_dataset
from torch.utils.data import DataLoader
from torch.functional import F
import re
from copy import deepcopy
from vit import ViT, MultiHeadSelfAttention

torch.set_default_dtype(torch.float64)

def vanilla_fusion(args: dict, model1: ViT, model2: ViT, fused: ViT) -> None:
    '''
    Vanilla fusion of two models. The weights of the two models are averaged without any alignment.

    Args:
        args (dict): The arguments of the program.
        model1 (ViT): The first model to be fused.
        model2 (ViT): The second model to be fused.
        fused (ViT): The fused model.
    '''
    alpha = args["fusion"]["alpha"]
    for (name1,param1), (name2,param2) in zip(model1.named_parameters(), model2.named_parameters()):
        new_param = (param1.data)*(1-alpha) + (param2.data)*alpha
        fused.state_dict()[name1].data.copy_(new_param)

def fusion(args: dict, model1: ViT, model2: ViT) -> ViT:
    '''
    Fuse the two models based on the given method.

    Args:
        args (dict): The arguments of the program.
        model1 (ViT): The first model to be fused.
        model2 (ViT): The second model to be fused.
    Returns:
        The fused model.
    '''
    torch.manual_seed(args["fusion"]["acts"]["dataloader_seed"])
    model1 = deepcopy(model1)
    model2 = deepcopy(model2)
    fused = deepcopy(model1)
    
    # Fuse the models
    if args["fusion"]["method"] == 'vanilla':
        vanilla_fusion(args, model1, model2, fused)
    else:
        model_fusion(args, model1, model2, fused)
    return fused

def get_activations(args: dict, model1: ViT, model2: ViT, train_loader: DataLoader) -> Tuple[dict[str, torch.Tensor], dict[str, torch.Tensor]]:
    """Run a mini-batch of image samples (size n) through the model and return the concatenated activations after all linear layers.
    (Optionally) Makes sure to only gather activations for images that are correctly classified by both models.

    Args:
        args (dict): The arguments of the program.
        model1 (ViT): First model to calculate activations for.
        model2 (ViT): Second model to calculate activations for.
        train_loader (DataLoader): Train loader to sample images from.

    Returns:
        dict[str, torch.Tensor]: Dictionary with {layer_name, averaged_activations} for each MLP layer.
    """
    
    activations = {}
    num_samples_processed = 0
    # Copy the two models to check for correct classification later on
    if args["fusion"]["acts"]["correct_inference_only"]:
        model1_copy = deepcopy(model1)
        model2_copy = deepcopy(model2)

    def get_hook(activation, name):
        def hook(module: nn.Linear, input, output):
            if name not in activation:
                activation[name] = output.detach()
            else:
                activation[name] = torch.hstack([activation[name], output.detach()])
        return hook
    
    for idx, model in enumerate([model1, model2]):
        activations[idx] = {}
        forward_hooks = []

        # Set forward hooks for all the layers
        for name, module in model.named_modules():
            if isinstance(module, nn.Linear) and 'fc' not in name:
                forward_hooks.append(module.register_forward_hook(get_hook(activations[idx], name)))
        
        # Set model to train mode
        model.train()
    
    for _, (data, target) in enumerate(train_loader):
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        model1.to(device)
        model2.to(device)
        # Could do an ablation study here, see what influence it has on the results to just take correctly classified results
        data = data.to(device)
        if args["fusion"]["acts"]["correct_inference_only"]:
            model1_copy.to(device)
            model2_copy.to(device)
            target = target.to(device)
            # Check if the models agree on the classification and it's correct
            # Need to use model copies to not invoke the hooks (since we may not want that data)
            if model1_copy(data).argmax(dim=1) != target or model2_copy(data).argmax(dim=1) != target:
                continue
            # Compute the activations
        model1(data)
        model2(data)
        num_samples_processed += 1
        if num_samples_processed == args["fusion"]["acts"]["num_samples"]:
            break

    # Might check influence to do some post-processing, standardizing/normalizing of the activations etc. (via standardize_activations -> didn't help as of now)

    for hook in forward_hooks:
        hook.remove()

    if args["fusion"]["acts"]["standardize"]:
        for name, activation in activations[0].items():
            mean_acts = torch.mean(activation, dim=1)
            std_acts  = torch.std(activation, dim=1)
            activations[0][name] = torch.div(torch.sub(activation, mean_acts), torch.add(std_acts, 1e-9))

        for name, activation in activations[1].items():
            mean_acts = torch.mean(activation, dim=1)
            std_acts  = torch.std(activation, dim=1)
            activations[1][name] = torch.div(torch.sub(activation, mean_acts), torch.add(std_acts, 1e-9))

    
    return activations

def model_fusion(args: dict, model1: ViT, model2: ViT, fused: ViT) -> None:
    '''
    Iterate through all model modules and align and fuse them based on the given method.

    Args:
        args (dict): The arguments of the program.
        model1 (ViT): Model which gets aligned.
        model2 (ViT): Anchor model.
        fused (ViT): The fused model.
    '''
    # If type includes activation, attach hooks to the layers and run activation on a small batch, gather the attention weights
    # and MLP activations per layer (where n is the batch size used)
    
    activations = {}
    if args["fusion"]["type"] == 'acts' or args['fusion']['residual']==2:
        train_data_set = load_dataset(train_set=True)
        train_loader = DataLoader(train_data_set, batch_size=1, shuffle=True, num_workers=0)
        
        # OT implementation calls them as dicts (see compute_activations.py line 81+)
        # Need to build a dict with the activations per model
        activations = get_activations(args, model1=model1, model2=model2, train_loader=train_loader)  
        
    
    # Initialize T_in which is the initial transformation matrix between the prior layer and the following layer
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    # TODO: [Task A] Change to the transport matrix received by embedding alignment
    T_in = torch.eye(model1.hidden).to(device)
    T_msa = torch.eye(model1.hidden).to(device)


    # Iterate over all layers of both models
    residual_counter = 0
    layer_counter = 0
    skip_activation_msa = 0
    skip_activation_mlp = 0

    alpha = args["fusion"]["alpha"]
    residual_bruteforce = args["fusion"]["residual_bruteforce"]
    if residual_bruteforce == 'None': residual_bruteforce = None

    residual = args["fusion"]["residual"]

    for layer1, layer2 in zip(model1.named_modules(), model2.named_modules()):
        name, module1 = layer1
        module2 = layer2[1]
        # Average the weight matrices for all the layers we want to fuse and not retrain
        if "mlp" in name and isinstance(module1, nn.Sequential):
            T_l2 = fully_connected_fusion(args, module1, module2, fused, name, activations, T_msa)
            
            # Choose whether to take residually passed or current T based on specific configuration passed e.g. [0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1] (for 7 layer network)
            if residual_bruteforce is not None:
                if residual_bruteforce[residual_counter] == 0:
                    T_in = T_l2
                else:
                    T_in = T_msa
                residual_counter += 1

            # Choose whether to take residually passed or current T based on the norm of the activations
            elif residual == 2:
                mlp_activation = activations[0][f"enc.{layer_counter}.mlp.3"]
                current_acts = mlp_activation
                previous_acts = skip_activation_msa
                if torch.norm(current_acts) > torch.norm(previous_acts):
                    T_in = T_l2
                else:
                    T_in = T_msa
                skip_activation_mlp = current_acts + previous_acts

            # Average the transport matrices at the skip connection according to the respective inputs
            elif residual == 1:
                T_in = torch.add(T_msa*0.5, T_l2*0.5)
                # Sanity check
                torch.testing.assert_close(T_in, (T_msa + T_l2)/2.0)
            else:
                T_in = T_l2

            # increase layer counter since we go into next encoder block
            layer_counter+=1

        
        # If the network has layer norms, permute the layers as well
        elif 'la1' in name and isinstance(module1, nn.LayerNorm):
            T_in = ln_fusion(args, module1, module2, fused, name, T_in)
        elif 'la2' in name and isinstance(module1, nn.LayerNorm):
            T_msa = ln_fusion(args, module1, module2, fused, name, T_msa)   
        elif 'fc' in name and isinstance(module1, nn.LayerNorm):
            T_in = ln_fusion(args, module1, module2, fused, name, T_in)


        elif isinstance(module1, MultiHeadSelfAttention):
            T_o_out = attention_fusion(args, module1=module1, module2=module2, model1=model1, 
                                                                    model2=model2, fused=fused, T_in=T_in, name=name, 
                                                                    attention_values=activations)
            
            # Average the transport matrices at the skip connection according to the respective inputs
            if residual_bruteforce is not None:
                if residual_bruteforce[residual_counter] == 0:
                    T_msa = T_o_out
                else:
                    T_msa = T_in
                residual_counter += 1

            elif residual == 2:
                msa_activation_1 = activations[0][f"enc.{layer_counter}.msa.o"]
                current_acts = msa_activation_1
                
                if layer_counter != 0:
                    mlp_activation = skip_activation_mlp
                else:
                    mlp_activation = torch.norm(activations[0][f"emb"])
                previous_acts = mlp_activation
                if torch.norm(current_acts) > torch.norm(previous_acts):
                    T_msa = T_o_out
                else:
                    T_msa = T_in
                if layer_counter==0:
                    skip_activation_msa = current_acts
                else:
                    skip_activation_msa = current_acts + previous_acts
                
            elif residual == 1:
                T_msa = torch.add(T_in*0.5, T_o_out*0.5)
                # Sanity check
                torch.testing.assert_close(T_msa, (T_in + T_o_out) / 2.0)
            else:
                T_msa = T_o_out

        elif 'fc' in name and isinstance(module1, nn.Linear):
            weight1 = module1.weight
            weight2 = module2.weight
            # propagate T_in to the next layer
            weight1 = T_in.T @ weight1.T
            module1.weight = nn.Parameter(weight1.T, requires_grad=False)
            fused_weight = (1 - alpha) * module1.weight.detach() + alpha * weight2.detach()
            # Assign the averaged weight matrix to the fused model
            # Set the gradients to none, so that the weights are not re-trained
            fused.get_submodule(name).weight = nn.Parameter(fused_weight, requires_grad=False)
    
        
    for (name1, param1), (name2, param2) in zip(model1.named_parameters(), model2.named_parameters()):
        if 'cls_token' in name1:
            fused_param = (1 - alpha) * param1.detach() + alpha * param2.detach()
            fused.get_parameter(name1).data = nn.Parameter(fused_param, requires_grad=False)
        if 'emb.weight' in name1:
            fused_param = (1 - alpha) * param1.detach() + alpha * param2.detach()
            fused.get_parameter(name1).data = nn.Parameter(fused_param, requires_grad=False)
        if 'pos_emb' in name1:
            fused_param = (1 - alpha) * param1.detach() + alpha * param2.detach()
            fused.get_parameter(name1).data = nn.Parameter(fused_param, requires_grad=False)


def fully_connected_fusion_weight(args: dict, module1: nn.Sequential, module2: nn.Sequential, fused: ViT, name: str, mlp_names: list, T_msa: torch.Tensor) -> torch.Tensor:
    '''
    Align the nodes of the MLPs and average their weight matrices.

    Args:
        args (dict): Arguments passed to the program
        module1 (nn.Sequential): Module which gets aligned
        module2 (nn.Sequential): Anchor module
        fused (ViT): The fused model
        name (str): The name of the module
        mlp_names (list): The names of the MLPs
        T_msa (torch.Tensor): The transport matrix of the previous layer

    Returns:
        torch.Tensor: The transport matrix of the current layer
    '''
    parser = parameters.get_parser()

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    if device.type == 'cuda':
        gpu_id = 0
    else:
        gpu_id = -1
    
    ot_args = parser.parse_args(['--gpu-id', f'{gpu_id}', '--correction', '--ground-metric', 'euclidean', '--weight-stats', \
                              '--geom-ensemble-type', 'wts', '--ground-metric-normalize', 'none', '--ground-metric-eff', \
                              '--recheck-cifar', '--exact', '--not-squared', '--past-correction', '--return-permutationmatrix', '--ensemble-step', f'{args["fusion"]["alpha"]}'])
    
    modules = [module1, module2]
    # permute first layer of muodule to keep embedding in place
    incoming_layer = module1.get_submodule(f"{mlp_names[0]}").weight
    incoming_layer = (T_msa.T @ incoming_layer.T).T
    module1.get_submodule(f"{mlp_names[0]}").weight = nn.Parameter(incoming_layer, requires_grad=False)

    fused_weight, T_l2 = wasserstein_ensemble.get_wassersteinized_layers_modularized(ot_args, modules)

    for idx, mlp_name in enumerate(mlp_names):
        fused.get_submodule(f"{name}.{mlp_name}").weight = nn.Parameter(fused_weight[idx], requires_grad=False)

    return T_l2[f"{mlp_names[-1]}.weight"]
    



def ln_fusion(args: dict, module1: nn.LayerNorm, module2: nn.LayerNorm, fused: ViT, name: str, T_in: torch.Tensor) -> torch.Tensor:
    '''
    Align the nodes of the layer norms and average their weights and biases.

    Args:
        args (dict): Arguments passed to the program
        module1 (nn.LayerNorm): Module which gets aligned
        module2 (nn.LayerNorm): Anchor module
        fused (ViT): The fused model
        name (str): The name of the module
        T_in (torch.Tensor): The transport matrix of the previous layer

    Returns:
        torch.Tensor: The transport matrix of the previous layer
    '''
    if module1.weight == None:
        return T_in
    norm1_weight = module1.weight.detach() @ T_in
    norm2_weight = module2.weight.detach()

    norm1_bias = module1.bias.detach() @ T_in
    norm2_bias = module2.bias.detach()

    alpha = args["fusion"]["alpha"]
    fused_norm_weight = (1 - alpha) * norm1_weight + alpha * norm2_weight
    fused_norm_bias = (1 - alpha) * norm1_bias + alpha * norm2_bias

    fused.get_submodule(name).weight = nn.Parameter(fused_norm_weight, requires_grad=False)
    fused.get_submodule(name).bias = nn.Parameter(fused_norm_bias, requires_grad=False)

    return T_in


def fully_connected_fusion_activation(args: dict, module1: nn.Linear, module2: nn.Linear, fused: models.VisionTransformer, name: str, mlp_names: list, T_msa: torch.Tensor, activations: dict[dict[str, torch.Tensor]]) -> torch.Tensor:
    '''
    Align the nodes of the MLPs based on their activations of a small training batch and average their weight matrices.

    Args:
        args (dict): Arguments passed to the program
        module1 (nn.Linear): Module which gets aligned
        module2 (nn.Linear): Anchor module
        fused (ViT): The fused model
        name (str): The name of the module
        mlp_names (list): The names of the MLPs
        T_msa (torch.Tensor): The transport matrix of the previous layer
        activations (dict[dict[str, torch.Tensor]]): The activations of the training batch

    Returns:
        torch.Tensor: The transport matrix of the current layer
    '''
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    if device.type == 'cuda':
        gpu_id = 0
    else:
        gpu_id = -1

    parser = parameters.get_parser()
    
    ot_args = parser.parse_args(['--gpu-id', f'{gpu_id}', '--correction', '--ground-metric', 'euclidean', '--weight-stats', \
                              '--geom-ensemble-type', 'acts', '--ground-metric-normalize', 'none', '--ground-metric-eff', \
                              '--recheck-cifar', '--exact', '--past-correction', '--return-permutationmatrix', \
                              '--not-squared', '--ensemble-step', f'{args["fusion"]["alpha"]}'])

    modules = [module1, module2]
    # permute input of first layer of module according to the output permutation of the previous layer
    incoming_layer = module1.get_submodule(f"{mlp_names[0]}").weight
    incoming_layer = (T_msa.T @ incoming_layer.T).T
    module1.get_submodule(f"{mlp_names[0]}").weight = nn.Parameter(incoming_layer, requires_grad=False)

    # Need to extract the mlp activations from the activations dict
    mlp_activations = {}
    for idx, _ in enumerate([module1, module2]):
        mlp_activations[idx] = {}
        for layer_name in activations[idx].keys():
            if name in layer_name and any([mlp_name in layer_name for mlp_name in mlp_names]):
                reduced_name = layer_name.split('.')[-1]
                mlp_activations[idx][reduced_name] = activations[idx][layer_name]
    fused_weight, T_mlps = wasserstein_ensemble.get_acts_wassersteinized_layers_modularized(ot_args, modules, activations=mlp_activations)

    T_l1 = T_mlps[f"{mlp_names[-2]}.weight"]
    T_l2 = T_mlps[f"{mlp_names[-1]}.weight"]
    mlp_activations = None

    # Permute activations of the mlp accordingly
    for act_name in activations[0].keys():
        if name in act_name:
            if act_name.split('.')[-1] == mlp_names[-2]:
                activations[0][act_name] = activations[0][act_name] @ T_l1
            elif act_name.split('.')[-1] == mlp_names[-1]:
                activations[0][act_name] = activations[0][act_name] @ T_l2
    
    for idx, mlp_name in enumerate(mlp_names):
        fused.get_submodule(f"{name}.{mlp_name}").weight = nn.Parameter(fused_weight[idx], requires_grad=False)

    return T_l2


def fully_connected_fusion(args: dict, module1: nn.Sequential, module2: nn.Sequential, fused: ViT, name: str, activations: dict[dict[str, torch.Tensor]], T_msa: torch.Tensor) -> torch.Tensor:
    '''
    Call the correct MLP fusion function based on the fusion type.

    Args:
        args (dict): Arguments passed to the program
        module1 (nn.Sequential): Model which gets aligned
        module2 (nn.Sequential): Anchor model
        fused (ViT): The fused model
        name (str): The name of the module
        activations (dict[dict[str, torch.Tensor]]): The activations of the training batch
        T_msa (torch.Tensor): The transport matrix of the previous layer

    Returns:
        torch.Tensor: The transport matrix of the current layer
    '''
    mlp_names = []
    for layer1 in module1.named_modules():
        name1 , mod1 = layer1
        if isinstance(mod1, nn.Linear):
            mlp_names.append(name1)

    if args["fusion"]["type"] == 'wts':
        T_l2 = fully_connected_fusion_weight(args, module1, module2, fused, name, mlp_names, T_msa)
    elif args["fusion"]["type"] == 'acts':
        T_l2 = fully_connected_fusion_activation(args, module1, module2, fused, name, mlp_names, T_msa, activations)
    return T_l2


def attention_fusion(args: dict, module1: MultiHeadSelfAttention, module2: MultiHeadSelfAttention, model1: ViT, model2: ViT, fused: ViT, 
                    T_in: torch.Tensor, name: str, attention_values: dict[str, torch.Tensor]) -> torch.Tensor:
    '''
    Call the correct attention fusion function based on the fusion method.

    Args:
        args (dict): Arguments passed to the program
        module1 (MultiHeadSelfAttention): Module which gets aligned
        module2 (MultiHeadSelfAttention): Anchor module 
        model1 (ViT): Model which gets aligned
        model2 (ViT): Anchor model
        fused (ViT): The fused model
        T_in (torch.Tensor): The transport matrix of the previous layer
        name (str): The name of the module
        attention_values (dict[str, torch.Tensor]): The attention values of the training batch

    Returns:
        torch.Tensor: The transport matrix of the current layer
    '''
 
    fused_module = fused.get_submodule(name)
    method = args["fusion"]["method"]
    if method == 'head_aware':
        T_o_out = head_aware_fusion(args, module1, module2, model1, model2, fused, T_in, name, activations=attention_values)
    if method == 'ignore_heads':
        T_o_out = ignore_heads_fusion(args, module1, module2, fused_module, T_in, name, activations=attention_values)
    if method == 'all_combinations':
        T_o_out = all_combinations_fusion(args, module1, module2, fused_module, T_in, name, activations=attention_values)
    
    return T_o_out
   

def head_aware_fusion(args: dict, module1: MultiHeadSelfAttention, module2: MultiHeadSelfAttention, model1: ViT, model2: ViT, fused: MultiHeadSelfAttention, 
                     T_in: torch.tensor,  name: str='', activations: dict[dict[str, torch.Tensor]] = {}) -> torch.Tensor:
    '''
    Align and fuse the heads of the attention modules. First, the heads are aligned, then they are aligned in-head.

    Args:
        args (dict): Arguments passed to the program
        module1 (MultiHeadSelfAttention): Module which gets aligned
        module2 (MultiHeadSelfAttention): Anchor module
        model1 (ViT): Model which gets aligned
        model2 (ViT): Anchor model
        fused (MultiHeadSelfAttention): The fused model
        T_in (torch.tensor): The transport matrix of the previous layer
        name (str, optional): The name of the module. Defaults to ''.
        activations (dict[dict[str, torch.Tensor]], optional): The activations of the training batch. Defaults to {}.

    Returns:
        torch.Tensor: The transport matrix of the current layer
    '''


    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    if device.type == 'cuda':
        gpu_id = 0
    else:
        gpu_id = -1
    ensemble_type = 'wts'
    if args["fusion"]["type"] == 'acts':
        ensemble_type = 'acts'
    parser = parameters.get_parser()
    ot_args = parser.parse_args(['--gpu-id', f'{gpu_id}', '--correction', '--ground-metric', 'euclidean', '--weight-stats', \
                            '--geom-ensemble-type', f'{ensemble_type}', '--ground-metric-normalize', 'none', '--ground-metric-eff', \
                            '--recheck-cifar', '--exact', '--past-correction', '--not-squared', '--return-permutationmatrix', '--ensemble-step', '0'])

    # Permute q k v prior to in head alignment
    q_weight = module1.q.weight
    k_weight = module1.k.weight
    v_weight = module1.v.weight
    T_in.to(device)
    q_weight = T_in.T @ q_weight.T
    k_weight = T_in.T @ k_weight.T
    v_weight = T_in.T @ v_weight.T
    module1.q.weight = nn.Parameter(q_weight.T, requires_grad=False)
    module1.k.weight = nn.Parameter(k_weight.T, requires_grad=False)
    module1.v.weight = nn.Parameter(v_weight.T, requires_grad=False)

    T_head = torch.eye(module1.head).to(device)
    T_o_out = torch.eye(module1.feats).to(device)

    # Align between heads
    T_head = align_heads(args, module1=module1, module2=module2, fused=fused, name=name, alpha=0, activations=activations) # alpha=0 becaus only aligning and not fusing
    
    # permute activations as the heads have been permuted
    if args["fusion"]["type"] == 'acts':
        for layer_name, act in activations[0].items():
            if name in layer_name and any(att in layer_name for att in ['q', 'k', 'v']):
                activations[0][layer_name] = activations[0][layer_name] @ T_head

    module1.q.weight = nn.Parameter(fused.get_submodule(name).q.weight.detach(), requires_grad=False)
    module1.k.weight = nn.Parameter(fused.get_submodule(name).k.weight.detach(), requires_grad=False)
    module1.v.weight = nn.Parameter(fused.get_submodule(name).v.weight.detach(), requires_grad=False)
    module1.o.weight = nn.Parameter(fused.get_submodule(name).o.weight.detach(), requires_grad=False)
        
    # Align in head
    in_head_alignment(args, module1=module1, module2=module2, fused_module=fused.get_submodule(name), name=name,activations=activations)

    module1.q.weight = nn.Parameter(fused.get_submodule(name).q.weight.detach(), requires_grad=False)
    module1.k.weight = nn.Parameter(fused.get_submodule(name).k.weight.detach(), requires_grad=False)
    module1.v.weight = nn.Parameter(fused.get_submodule(name).v.weight.detach(), requires_grad=False)
    module1.o.weight = nn.Parameter(fused.get_submodule(name).o.weight.detach(), requires_grad=False)
    if args["fusion"]["type"] == 'acts':
        o_activations = {}
        for idx, _ in enumerate([module1, module2]):
            o_activations[idx] = {}
            o_activations[idx]['weight'] = activations[idx][name + '.o']
        aligned_o, T_o_out_dict = wasserstein_ensemble.get_acts_wassersteinized_layers_modularized(ot_args ,[module1.o, module2.o], activations=o_activations)
        o_activations = None
    else:
        aligned_o, T_o_out_dict = wasserstein_ensemble.get_wassersteinized_layers_modularized(ot_args ,[module1.o, module2.o])
    
    # We have to keep track of the total output dimension permutation of the head aware approach
    T_o_out = T_o_out @ T_o_out_dict['weight']
    module1.o.weight = nn.Parameter(aligned_o[0], requires_grad=False)

    # simple averaging of MSA weights
    alpha = args["fusion"]["alpha"]
    W_q_fused = (1 - alpha) * module1.q.weight + alpha * module2.q.weight
    W_k_fused = (1 - alpha) * module1.k.weight + alpha * module2.k.weight
    W_v_fused = (1 - alpha) * module1.v.weight + alpha * module2.v.weight
    W_o_fused = (1 - alpha) * module1.o.weight + alpha * module2.o.weight
    fused.get_submodule(name).q.weight = nn.Parameter(W_q_fused, requires_grad=False)
    fused.get_submodule(name).k.weight = nn.Parameter(W_k_fused, requires_grad=False)
    fused.get_submodule(name).v.weight = nn.Parameter(W_v_fused, requires_grad=False)
    fused.get_submodule(name).o.weight = nn.Parameter(W_o_fused, requires_grad=False)
    return T_o_out    

def all_combinations_fusion(args: dict, module1: MultiHeadSelfAttention, module2: MultiHeadSelfAttention, fused_module: MultiHeadSelfAttention,
                                T_in: torch.Tensor, name: str,activations: dict[str, torch.Tensor] = {}) -> torch.Tensor:
    '''
    Fuse two MSA modules based on a specific head combination. 

    Args:
        args: arguments of the experiment
        module1: MSA module which gets aligned
        module2: anchor MSA module
        fused: fused MSA module
        T_in: permutation matrix of the input dimension
        name: name of the MSA module
        activations: activations of the MSA module

    Returns:
        T_o_out: permutation matrix of the output dimension
    '''  
    parser = parameters.get_parser()
    # Permute q k v prior to in head alignment
    q_weight = module1.q.weight
    k_weight = module1.k.weight
    v_weight = module1.v.weight
    q_weight = T_in.T @ q_weight.T
    k_weight = T_in.T @ k_weight.T
    v_weight = T_in.T @ v_weight.T
    module1.q.weight = nn.Parameter(q_weight.T, requires_grad=False)
    module1.k.weight = nn.Parameter(k_weight.T, requires_grad=False)
    module1.v.weight = nn.Parameter(v_weight.T, requires_grad=False)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    current_combination = args["fusion"]["combination"][0]
    permuted_module = get_specific_head_permutation(args, module1, current_combination, activations, name)
    in_head_alignment(args, permuted_module, module2, fused_module, name=name, activations=activations)
    if device.type == 'cuda':
        gpu_id = 0
    else:
        gpu_id = -1

    if args["fusion"]["type"] == 'acts':
        ot_args = parser.parse_args(['--gpu-id', f'{gpu_id}', '--correction', '--ground-metric', 'euclidean', '--weight-stats', \
                                '--geom-ensemble-type', 'acts', '--ground-metric-normalize', 'none', '--ground-metric-eff', \
                                '--recheck-cifar', '--exact', '--past-correction', '--not-squared', '--return-permutationmatrix', '--ensemble-step', '0'])
        o_activations = {}
        for idx, _ in enumerate([fused_module, module2]):
            o_activations[idx] = {}
            o_activations[idx]['weight'] = activations[idx][name + '.o']
        aligned_o, T_o_out_dict = wasserstein_ensemble.get_acts_wassersteinized_layers_modularized(ot_args ,[fused_module.o, module2.o], activations=o_activations)
        o_activations = None
    else:
        
        ot_args = parser.parse_args(['--gpu-id', f'{gpu_id}', '--correction', '--ground-metric', 'euclidean', '--weight-stats', \
                                '--geom-ensemble-type', 'wts', '--ground-metric-normalize', 'none', '--ground-metric-eff', \
                                '--recheck-cifar', '--exact', '--past-correction', '--not-squared', '--return-permutationmatrix', '--ensemble-step', '0'])    
        aligned_o, T_o_out_dict = wasserstein_ensemble.get_wassersteinized_layers_modularized(ot_args ,[fused_module.o, module2.o])

    fused_module.o.weight = nn.Parameter(aligned_o[0], requires_grad=False)
    T_o_out = T_o_out_dict['weight']
    permuted_module = deepcopy(fused_module)

    alpha = args["fusion"]["alpha"]
    q_weight = (1-alpha) * fused_module.q.weight.detach() + alpha * module2.q.weight.detach()
    k_weight = (1-alpha) * fused_module.k.weight.detach() + alpha * module2.k.weight.detach()
    v_weight = (1-alpha) * fused_module.v.weight.detach() + alpha * module2.v.weight.detach()
    o_weight = (1-alpha) * fused_module.o.weight.detach() + alpha * module2.o.weight.detach()
    fused_module.q.weight = nn.Parameter(q_weight, requires_grad=False)
    fused_module.k.weight = nn.Parameter(k_weight, requires_grad=False)
    fused_module.v.weight = nn.Parameter(v_weight, requires_grad=False)
    fused_module.o.weight = nn.Parameter(o_weight, requires_grad=False)

    args["fusion"]["combination"].pop(0)
    return T_o_out


def ignore_heads_fusion(args: dict, module1: MultiHeadSelfAttention, module2: MultiHeadSelfAttention, fused_module: MultiHeadSelfAttention, 
                     T_in: torch.tensor, name: str='', activations: dict[dict[str, torch.Tensor]] = {}) -> torch.Tensor:
    
    '''
    Fuse two MSA modules without between head alignment. The in head alignment is done without splitting the matrices into heads.

    Args:
        args: arguments of the experiment
        module1: MSA module which gets aligned
        module2: anchor MSA module
        fused: fused MSA module
        T_in: permutation matrix of the input dimension
        name: name of the MSA module
        activations: activations of the MSA module

    Returns:
        T_o_out: permutation matrix of the output dimension
    '''
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    if device.type == 'cuda':
        gpu_id = 0
    else:
        gpu_id = -1
    ensemble_type = args["fusion"]["type"]
    parser = parameters.get_parser()
    ot_args = parser.parse_args(['--gpu-id', f'{gpu_id}', '--correction', '--ground-metric', 'euclidean', '--weight-stats', \
                            '--geom-ensemble-type', f'{ensemble_type}', '--ground-metric-normalize', 'none', '--ground-metric-eff', \
                            '--recheck-cifar', '--exact', '--not-squared', '--past-correction', '--return-permutationmatrix', '--ensemble-step', '0'])
     

    # Permute q k v prior to in head alignment
    q_weight = module1.q.weight
    k_weight = module1.k.weight
    v_weight = module1.v.weight
    T_in.to(device)
    q_weight = T_in.T @ q_weight.T
    k_weight = T_in.T @ k_weight.T
    v_weight = T_in.T @ v_weight.T
    module1.q.weight = nn.Parameter(q_weight.T, requires_grad=False)
    module1.k.weight = nn.Parameter(k_weight.T, requires_grad=False)
    module1.v.weight = nn.Parameter(v_weight.T, requires_grad=False)

    # align
    in_head_alignment(args, module1=module1, module2=module2, fused_module=fused_module, name=name, ignore_heads=True, activations=activations)

    if args["fusion"]["type"] == 'acts':
                o_activations = {}
                for idx, _ in enumerate([module1, module2]):
                    o_activations[idx] = {}
                    o_activations[idx]['weight'] = activations[idx][name + '.o']
                aligned_o, T_o_out_dict = wasserstein_ensemble.get_acts_wassersteinized_layers_modularized(ot_args ,[fused_module.o, module2.o], activations=o_activations)
                o_activations = None

    else:
        aligned_o, T_o_out_dict = wasserstein_ensemble.get_wassersteinized_layers_modularized(ot_args ,[fused_module.o, module2.o])

    T_o_out = T_o_out_dict['weight']
    fused_module.o.weight = nn.Parameter(aligned_o[0], requires_grad=False)

    # fuse
    alpha = args["fusion"]["alpha"]
    W_q_fused = (1 - alpha) * fused_module.q.weight + alpha * module2.q.weight
    W_k_fused = (1 - alpha) * fused_module.k.weight + alpha * module2.k.weight
    W_v_fused = (1 - alpha) * fused_module.v.weight + alpha * module2.v.weight
    W_o_fused = (1 - alpha) * fused_module.o.weight + alpha * module2.o.weight
    fused_module.q.weight = nn.Parameter(W_q_fused, requires_grad=False)
    fused_module.k.weight = nn.Parameter(W_k_fused, requires_grad=False)
    fused_module.v.weight = nn.Parameter(W_v_fused, requires_grad=False)
    fused_module.o.weight = nn.Parameter(W_o_fused, requires_grad=False)
    return T_o_out


# DEBUGGING ---------------------------------------------------------------------------

def main():
    pass
    

if __name__ == '__main__':
    main()

