import torch
import torch.nn.functional as F
from train import load_dataset
from torch.utils.data import DataLoader
from tqdm import tqdm
import time
import csv


def ensemble_output(networks, image) -> torch.Tensor:
    outputs = []
    for model in networks:
        output = model(image)
        outputs.append(output)
    # Average outputs of models
    output = torch.mean(torch.stack(outputs), dim=0)
    return output

def ensemble(networks, save_path, csv_details=None):
    # Load the test dataset
    test_dataset = load_dataset(train_set=False)
    device = torch.device("cuda" if torch.cuda.is_available() else ("mps" if torch.backends.mps.is_available() else "cpu"))
    test_loader = DataLoader(test_dataset, batch_size=1, shuffle=False, num_workers=0)

    for model in networks:
        # Set the model to evaluation mode
        model.eval()
        model.to(device)

        all_corrects, all_samples = 0, 0
        
    with torch.no_grad():
        for image, target in tqdm(test_loader):
            image = image.to(device)
            target = target.to(device)
            # Forward pass
            output = ensemble_output(networks, image)
            # Get the predictions
            _, pred = torch.max(output, 1)
            # Store the number of correct predictions
            correct = torch.sum(pred == target)
            # Update the number of correct predictions and samples
            all_corrects += correct
            all_samples += 1

        # Compute the test accuracy for the epoch
        test_acc = float(all_corrects / all_samples)
        csv_details.append(test_acc)

        with open('plots/refactoring.csv', 'a', newline='') as file:
            writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC)
            # Write some data to the CSV file
            writer.writerow(csv_details)

        # Update the progress bar
        print(f"Test acc: {test_acc:.4f}")


    # Write the test accuracy to a file
    with open(save_path, "a+") as f:
        current_time = time.localtime()
        current_time = time.strftime('%a, %d %b %Y %H:%M:%S GMT', current_time)
        f.write(f"{current_time}: Test acc for ensemble: \t {test_acc:.4f}")


def main():
    # Call this via the evaluation.py script
    raise NotImplementedError

if __name__ == "__main__":
    main()



