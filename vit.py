import torch
import torch.nn as nn
from typing import Mapping, Any
import torch.nn.functional as F

# TODO: [Task J] Check that this ViT is identical to the one we actually train

class ViT(nn.Module):
    def __init__(self, args, in_c:int=3, num_classes:int=10, img_size:int=32, patch:int=8, head:int=8, is_cls_token:bool=True):
        super(ViT, self).__init__()
        # hidden=384

        # access to the attributes
        self.num_classes = num_classes
        self.img_size = img_size
        self.dropout = args["model"]["dropout"]
        self.num_layers = args["model"]["num_layers"]
        self.hidden = args["model"]["hidden"]
        self.mlp_hidden = args["model"]["mlp_hidden"]
        self.head = head
        self.is_cls_token = is_cls_token

        self.patch = patch # number of patches in one row(or col)
        self.is_cls_token = is_cls_token
        self.patch_size = img_size//self.patch
        f = (img_size//self.patch)**2*3 # 48 # patch vec length
        num_tokens = (self.patch**2)+1 if self.is_cls_token else (self.patch**2)

        # Change embedding layer here
        self.emb = nn.Linear(f, self.hidden) # (b, n, f)
        # Change the CLS token here
        self.cls_token = nn.Parameter(torch.randn(1, 1, self.hidden)) if is_cls_token else None
        # Change positional embedding here
        self.pos_emb = nn.Parameter(torch.randn(1,num_tokens, self.hidden))
        enc_list = [TransformerEncoder(args, self.hidden,mlp_hidden=self.mlp_hidden, dropout=self.dropout, head=head) for _ in range(self.num_layers)]
        self.enc = nn.Sequential(*enc_list)
        self.fc = nn.Sequential(
            # We deactivate the learnables in the layer norm to make the parents more similar
            nn.LayerNorm(self.hidden, elementwise_affine=args["model"]["layer_norm"]),
            nn.Linear(self.hidden, num_classes) # for cls_token
        )

    def load_state_dict(self, state_dict: Mapping[str, Any], strict: bool = True):
        if "model" in next(iter(state_dict)):
            state_dict_renamed = {key.replace("model.", ""): value for key, value in state_dict.items()}
        else:
            state_dict_renamed = state_dict
        return super().load_state_dict(state_dict_renamed, strict)

    def forward(self, x):
        out = self._to_words(x)
        out = self.emb(out)
        if self.is_cls_token:
            out = torch.cat([self.cls_token.repeat(out.size(0),1,1), out],dim=1)
        out = out + self.pos_emb
        out = self.enc(out)
        if self.is_cls_token:
            out = out[:,0]
        else:
            out = out.mean(1)
        out = self.fc(out)
        return out

    def _to_words(self, x):
        """
        (b, c, h, w) -> (b, n, f)
        """
        out = x.unfold(2, self.patch_size, self.patch_size).unfold(3, self.patch_size, self.patch_size).permute(0,2,3,4,5,1)
        out = out.reshape(x.size(0), self.patch**2 ,-1)
        return out
    
class TransformerEncoder(nn.Module):
    def __init__(self, args: dict, feats:int, mlp_hidden:int, head:int=8, dropout:float=0.):
        super(TransformerEncoder, self).__init__()
        self.args = args
        self.la1 = nn.LayerNorm(feats, elementwise_affine=args["model"]["layer_norm"])
        self.msa = MultiHeadSelfAttention(feats, head=head, dropout=dropout)
        self.la2 = nn.LayerNorm(feats, elementwise_affine=args["model"]["layer_norm"])
        self.mlp = nn.Sequential(
            nn.Linear(feats, mlp_hidden),
            nn.GELU(),
            nn.Dropout(dropout),
            nn.Linear(mlp_hidden, feats),
            nn.GELU(),
            nn.Dropout(dropout),
        )

    def forward(self, x):
        if self.args["model"]["residual"]:
            out = self.msa(self.la1(x)) + x
            out = self.mlp(self.la2(out)) + out
        else:
            out = self.msa(self.la1(x))
            out = self.mlp(self.la2(out))
        return out

class MultiHeadSelfAttention(nn.Module):
    def __init__(self, feats:int, head:int=8, dropout:float=0.):
        super(MultiHeadSelfAttention, self).__init__()
        self.head = head
        self.feats = feats
        self.sqrt_d = self.feats**0.5

        self.q = nn.Linear(feats, feats)
        self.k = nn.Linear(feats, feats)
        self.v = nn.Linear(feats, feats)

        self.o = nn.Linear(feats, feats)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        b, n, f = x.size()
        q = self.q(x).view(b, n, self.head, self.feats//self.head).transpose(1,2)
        k = self.k(x).view(b, n, self.head, self.feats//self.head).transpose(1,2)
        v = self.v(x).view(b, n, self.head, self.feats//self.head).transpose(1,2)

        score = F.softmax(torch.einsum("bhif, bhjf->bhij", q, k)/self.sqrt_d, dim=-1) #(b,h,n,n)
        attn = torch.einsum("bhij, bhjf->bihf", score, v) #(b,n,h,f//h)
        o = self.dropout(self.o(attn.flatten(2)))
        return o

class MultiHeadDepthwiseSelfAttention(nn.Module):
    def __init__(self, feats:int, head:int=8, dropout:float=0):
        super(MultiHeadDepthwiseSelfAttention, self).__init__()
        ...

    def forward(self, x):
        
        ...


if __name__ == "__main__":
    b,c,h,w = 4, 3, 32, 32
    x = torch.randn(b, c, h, w)
    net = ViT(in_c=c, num_classes= 10, img_size=h, patch=16, dropout=0.1, num_layers=7, hidden=384, head=12, mlp_hidden=384, is_cls_token=False)
    # out = net(x)
    # out.mean().backward()
    # print(out.shape)
    