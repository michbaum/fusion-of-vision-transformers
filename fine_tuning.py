import torch
import torch.nn as nn
from tqdm import tqdm

from torch.utils.data import DataLoader, random_split
from torchvision import datasets, transforms, models

from train import train
from fusion import fusion

FINE_TUNE_LEARNING_RATE = 0.0001
FINE_TUNE_EPOCHS = 5

def fine_tune_network(model: models.VisionTransformer, save = False, save_path = None, upsampling = False):
    # Enable all gradients of the network again
    for name, param in model.named_parameters():
        if not 'bias' in name:
            param.requires_grad = True
    
    raise NotImplementedError

def main():
    # Functions should only be called in evaluations.py
    raise NotImplementedError

if __name__ == "__main__":
    main()