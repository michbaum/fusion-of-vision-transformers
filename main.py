import os
import yaml
import sys
import logging
from evaluation import evaluate_all_combinations, evaluate_all_fusion, evaluate_residual_bruteforce, evaluate_mode_connectivity
from fusion import fusion

def load_args(log, EXPERIMENT_CFG_FILE, exp = None):
    """
    There are three ways in which an experiment can be defined. Below is a list ordered by priority (only experiment with highest priority is carried out)
    1. Main function input parameter 'exp'
    2. Command line specified
    3. Default experiment
    """
    if exp == None:
        if len(sys.argv) > 1:
            indices = [sys.argv.index(string) for string in sys.argv if '.yaml' in string]
            if (len(indices) > 0):
                assert(len(indices) == 1) # cannot specify multiple yaml files!
                EXPERIMENT_CFG_FILE = 'experiments/{0}'.format(sys.argv[indices[0]])
                log.info(" Running command line specified experiment: {0}".format(EXPERIMENT_CFG_FILE))
            else:
                log.info(" Using predefined experiment: {0}".format(EXPERIMENT_CFG_FILE))
        else:
            log.info(" Using predefined experiment: {0}".format(EXPERIMENT_CFG_FILE))
    else:
        EXPERIMENT_CFG_FILE = 'experiments/{0}'.format(exp)
        log.info(" Using experiment file defined by main function input parameter: {0}".format(EXPERIMENT_CFG_FILE))
    log.info(" ------- Reading Experiment Configuration -------\n")
    cfg_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), EXPERIMENT_CFG_FILE.split("/")[0], EXPERIMENT_CFG_FILE.split("/")[1])
    with open(cfg_file, 'r') as f:
        args = yaml.safe_load(f)
    return args

def log_args(log, args):
    log.info('\n{0}'.format(yaml.dump(args, indent=4)))


def main(exp = None, log_file = None):
    EXPERIMENT_CFG_FILE = 'experiments/default.yaml'
    LOGGING_LEVEL = logging.INFO
    # Initialize logger
    if len(sys.argv) > 1:
        if (any('--debug' in string for string in sys.argv)):
            LOGGING_LEVEL = logging.DEBUG
    if log_file != None:
        log = logging.getLogger('{0}_main'.format(log_file))
        fileHandler = logging.FileHandler(log_file, mode='a')
        log.addHandler(fileHandler)
    else:
        log = logging.getLogger('main')
    logging.basicConfig(level=LOGGING_LEVEL)

    args = load_args(log=log, EXPERIMENT_CFG_FILE = EXPERIMENT_CFG_FILE, exp = exp)

    # Print experiment configuration to commandline
    log_args(log = log, args = args)

    if args['evaluation']['evaluation_method'] == "bruteforce_head_alignment" and args['fusion']['methods'] != ['head_aware']:
        raise ValueError("Bruteforce head alignment can only be used with head_aware fusion method. Set in the config file fusion methods to ['head_aware']")
    
    evaluate_all_fusion(args=args)



if __name__ == "__main__":
    main()