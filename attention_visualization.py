import matplotlib.pyplot as plt
import numpy as np
import PIL

import torch
import torch.nn.functional as F
import torchvision
import torchvision.transforms as T
from torchvision import datasets, transforms, models
from train import load_dataset
from networks import get_small_vit

from networks import get_vit_b16_without_MLP_bias
from copy import deepcopy
from fusion import fusion

from torch import nn


def load_dataset_no_transform(train_set: bool = True, upsampling: bool = False):

    assert not upsampling # upsampling is because of our new models not needed and therefore not supported anymore

    train_dataset = datasets.CIFAR10(
                '../cifar_data/',
                train=train_set,
                download=True,
                transform=transforms.Compose(
                    [transforms.ToTensor(),]
                )
    )
    return train_dataset

def get_activations(model, input):
    data, target = input
    activations = {}
    # Copy the two models to check for correct classification later on
    model_copy = deepcopy(model)

    def get_hook(activation, name):
        def hook(module: nn.Linear, input, output):
            if name not in activation:
                activation[name] = output.detach()
                #print(f"One activation shape is: {output.shape}")
            else:
                # activation[name] = (num_samples_processed * activation[name] + output.detach()) / (num_samples_processed + 1)
                activation[name] = torch.hstack([activation[name], output.detach()])
                #print(f"Concatenated activation shape for {name} is: {activation[name].shape}")
        return hook
    
    activations = {}
    forward_hooks = []

    # Set forward hooks for all the layers
    for name, module in model.named_modules():
        # TODO: If we want to align embeddings, might need to gather activations here
        if isinstance(module, nn.Linear) and 'fc' not in name and 'emb' not in name:
            # TODO: [Michael] This will attach hooks to q, k, v, o and the two mlp layers as well as the final fc layer
            #       The final fc layer is a bit unnecessary, since we won't align it
            #print(f"Attach hook to {name}")
            # TODO: [Michael] Sidak has another step inbetween here, check compute_activations l 187 if this fails
            forward_hooks.append(module.register_forward_hook(get_hook(activations, name)))
        
        # Set model to train mode
        model.train()

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model.to(device)
    model_copy.to(device)
    # TODO: [Michael] Could do an ablation study here, see what influence it has on the results to just take correctly classified results
    data = data.to(device)
    data = data.unsqueeze(0)
    target = torch.tensor([target]).to(device)

    # Check if the models agree on the classification and it's correct
    # Need to use model copies to not invoke the hooks (since we may not want that data)
    # if model_copy(data).argmax(dim=1) != target:
    #     raise ValueError("Model does not agree on the classification")
        # Compute the activations
    model(data)
    # img_show = data[0].permute(1, 2, 0)
    # plt.imshow(img_show)

    for hook in forward_hooks:
        hook.remove()

    standardize = False
    if standardize:
        #print("standardize activations")
        for name, activation in activations.items():
            mean_acts = torch.mean(activation, dim=1)
            std_acts  = torch.std(activation, dim=1)
            activations[name] = torch.div(torch.sub(activation, mean_acts), torch.add(std_acts, 1e-9))
            # activations[0][name] = torch.sub(activation, mean_acts)
    return activations


def get_attention_maps(model, input, layer, num_layers, name):
    activations = get_activations(model, input)

    module = model.enc[layer].msa
    number_heads = module.head
    hidden = module.feats
    columns_per_head = int(hidden/number_heads)

    layer_name = f"enc.{layer}.msa"
    q_name = layer_name + '.q'
    Q = activations[q_name]
    Q_heads = Q.reshape(Q.shape[1], number_heads, columns_per_head).permute(1,0,2)
    k_name = layer_name + '.k'
    K = activations[k_name]
    K_heads = K.reshape(K.shape[1], number_heads, columns_per_head).permute(1,0,2)

    sqrt_d = hidden**0.5
    attentions = []
    for i in range(number_heads):
        output = F.softmax(((Q_heads[i]@K_heads[i].T) / sqrt_d), dim=-1)
        attention = output[0,1:].reshape(8,8).cpu().detach().numpy()
        # print(attention)
        attentions.append(attention)
        # print("head", i)
        # print(attention)
        # plt.imshow(attention, cmap='inferno')
        # print("done")

    # fig.add_axes()
    if model.enc[0].msa.head == 12:
        fig, axes = plt.subplots(3, 4)
    elif model.enc[0].msa.head == 4:
        fig, axes = plt.subplots(2, 2)
    else:
        fig, axes = plt.subplots(1, 2)
    fig.suptitle(f"Visualization of Attention for Layer {layer} of {name} with {num_layers} Layers", fontsize=10)

    axs = axes.flatten()
    for img, ax in zip(attentions, axs):
        ax.set_xticks([])
        ax.set_yticks([])
        ax.imshow(img)

    # Adjust the spacing between subplots
    fig.tight_layout()

    # Save the figure as an image file

    plt.savefig(f"attention_plots/{num_layers}_layer_{layer}_{name}.png")
    plt.close(fig)
      

def main():
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    # Load the models
    # seed1 = 11
    # seed2= 12
    # num_heads = 12
    # hidden = 384
    # mlp_hidden = 1536
    # num_layers = 7
    # model1 = get_small_vit(heads=num_heads, hidden=hidden, mlp_hidden=mlp_hidden, num_layers=num_layers)
    # model2 = get_small_vit(heads=num_heads, hidden=hidden, mlp_hidden=mlp_hidden, num_layers=num_layers)
    # model1_name = f"vit_c10_seed{seed1}_aa_ls_nheads_{num_heads}_hidden_{hidden}"
    # model2_name = f"vit_c10_seed{seed2}_aa_ls_nheads_{num_heads}_hidden_{hidden}"

    seed1 = 21
    seed2= 22
    num_heads = 4
    hidden = 384
    mlp_hidden = 1536
    num_layers = 2
    model1 = get_small_vit(heads=num_heads, hidden=hidden, mlp_hidden=mlp_hidden, num_layers=num_layers)
    model2 = get_small_vit(heads=num_heads, hidden=hidden, mlp_hidden=mlp_hidden, num_layers=num_layers)
    model1_name = f"vit_c10_seed{seed1}_aa_ls_nheads_{num_heads}_no_res_hidden_{hidden}"
    model2_name = f"vit_c10_seed{seed2}_aa_ls_nheads_{num_heads}_no_res_hidden_{hidden}"


    model1.load_state_dict(torch.load(f'networks/{model1_name}.pth', map_location=device))
    model2.load_state_dict(torch.load(f'networks/{model2_name}.pth', map_location=device))
    model1.to(device)
    model2.to(device)
    
    image_idx = 20
    dat_raw = load_dataset_no_transform(train_set=False, upsampling=False)
    img_raw = dat_raw[image_idx][0].to("cpu").permute(1, 2, 0)
    data = load_dataset(train_set=False)

    # layer = 5
    # get_attention_maps(model1, data[image_idx], layer=layer, num_layers=num_layers, name="model1")

    aligned = fusion(deepcopy(model1), model2, method='bootstrap_inference', iterations=1, residual=False, ov_circuit=False, ov_combined=False, head_method='hungarian',
                                                    head_align_ov_combined=False, head_align_ov_stacked=False, n=30, alpha=0)

    fused_bootstrap = fusion(deepcopy(model1), model2, method='bootstrap_inference', iterations=1, residual=False, ov_circuit=False, ov_combined=False, head_method='hungarian',
                                                       head_align_ov_combined=False, head_align_ov_stacked=False, n=30, alpha=0.5)
    

    fused_dummy = fusion(deepcopy(model1), model2, method='dummy', iterations=1, residual=False, ov_circuit=False, ov_combined=False, head_method='hungarian',
                                                    head_align_ov_combined=False, head_align_ov_stacked=False, n=30, alpha=0.5)

    for layer in range(num_layers):
        get_attention_maps(model1, data[image_idx], layer=layer, num_layers=num_layers, name="model1")
        get_attention_maps(model2, data[image_idx], layer=layer, num_layers=num_layers, name="model2")
        get_attention_maps(aligned, data[image_idx], layer=layer, num_layers=num_layers, name="aligned")
        get_attention_maps(fused_bootstrap, data[image_idx], layer=layer, num_layers=num_layers, name="fused_bootstrap")
        get_attention_maps(fused_dummy, data[image_idx], layer=layer, num_layers=num_layers, name="fused_dummy")





if __name__ == '__main__':
    main()
