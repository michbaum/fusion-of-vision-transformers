from typing import List, Tuple
from vit import ViT, MultiHeadSelfAttention
from otfusion_sidak import wasserstein_ensemble, parameters
from otfusion_sidak.ground_metric import GroundMetric
import torch
from torch import nn

def split_heads(args: dict, module1: MultiHeadSelfAttention, module2: MultiHeadSelfAttention, ignore = False) -> Tuple[List[nn.Linear], List[nn.Linear], List[nn.Linear], List[nn.Linear]]:
    
    """Splits the attention module into its heads and concatenates the q and k matrices for every head. Optionally also concatenates the v and o matrices to build the OV circuit.

    Args:
        args (dict): Arguments from the config file.
        module1 (MultiHeadSelfAttention): First attention module to split.
        module2 (MultiHeadSelfAttention): Second attention module to split.
    Returns:
        Tuple[nn.Linear, nn.Linear, nn.Linear, nn.Linear]: Returns new linear networks for the 
        concatenated q and k matrices (optionally the don't concatenated them) and split v matrix.
    """
    # amount of heads 
    if ignore:
        number_heads = 1
    else:
        number_heads = module1.head
    hidden = module1.feats
    #head dimension
    dim_head = hidden // number_heads
    # access every head of q and k and reshape s.t. for every head a weight tensor
    q1 = module1.q.weight.view(number_heads, dim_head, -1)
    q2 = module2.q.weight.view(number_heads, dim_head, -1)
    k1 = module1.k.weight.view(number_heads, dim_head, -1)
    k2 = module2.k.weight.view(number_heads, dim_head, -1)
    v1 = module1.v.weight.view(number_heads, dim_head, -1)
    v2 = module2.v.weight.view(number_heads, dim_head, -1)

    qk1 = []
    qk2 = []
    v_1 = []
    v_2 = []

    q_1 = []
    q_2 = []
    k_1 = []
    k_2 = []
    
    no_qk_stacking = args["fusion"]["no_qk_stacking"]

    # concat heads of q and k and save into new linear network
    for i in range(number_heads):
        if no_qk_stacking:
            q1_head = q1[i]
            q2_head = q2[i]
            q_lin1 = nn.Linear(dim_head, hidden, bias=False)
            q_lin2 = nn.Linear(dim_head, hidden, bias=False)
            k1_head = k1[i]
            k2_head = k2[i]
            k_lin1 = nn.Linear(dim_head, hidden, bias=False)
            k_lin2 = nn.Linear(dim_head, hidden, bias=False)
            q_lin1.weight = nn.Parameter(q1_head)
            q_lin2.weight = nn.Parameter(q2_head)
            k_lin1.weight = nn.Parameter(k1_head)
            k_lin2.weight = nn.Parameter(k2_head)

            q_1.append(q_lin1)
            q_2.append(q_lin2)
            k_1.append(k_lin1)
            k_2.append(k_lin2)
        else:
            qk1_head = torch.cat((q1[i], k1[i]), dim=1)
            qk2_head = torch.cat((q2[i], k2[i]), dim=1)
            qk_lin1 = nn.Linear(dim_head, 2*hidden, bias=False)
            qk_lin2 = nn.Linear(dim_head, 2*hidden, bias=False)

            # add weights to networks
            qk_lin1.weight = nn.Parameter(qk1_head)
            qk_lin2.weight = nn.Parameter(qk2_head)

            qk1.append(qk_lin1)
            qk2.append(qk_lin2)

        # Only extract v per head
        v1_head = v1[i]
        v2_head = v2[i]
        v_lin1 = nn.Linear(dim_head, hidden, bias=False)
        v_lin2 = nn.Linear(dim_head, hidden, bias=False)
        
        # add weights to networks
        v_lin1.weight = nn.Parameter(v1_head)
        v_lin2.weight = nn.Parameter(v2_head)

        v_1.append(v_lin1)
        v_2.append(v_lin2)

    if no_qk_stacking:
        return q_1, q_2, k_1, k_2, v_1, v_2
    else: 
        return qk1, qk2, v_1, v_2