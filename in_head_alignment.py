from typing import List, Tuple
from vit import ViT, MultiHeadSelfAttention
from otfusion_sidak import wasserstein_ensemble, parameters
from otfusion_sidak.ground_metric import GroundMetric
import torch
from torch import nn
from split_heads import split_heads

def in_head_alignment(args, module1: MultiHeadSelfAttention, module2: MultiHeadSelfAttention, fused_module: MultiHeadSelfAttention, name:str,
                       ignore_heads: bool = False, activations: dict[dict[str, torch.Tensor]] = {}) -> Tuple[torch.Tensor, torch.Tensor]:
    '''
    Align each head (in-head) with wasserstein and fuse the two modules.
    
    Args:
        args (dict): Arguments from the config file.
        module1 (MultiHeadSelfAttention): Module which gets aligned.
        module2 (MultiHeadSelfAttention): Anchor module.
        fused_module (MultiHeadSelfAttention): Module which gets aligned.
        name (str): Name of the module.
        ignore_heads (bool, optional): If True, the matrices are not split into heads. Defaults to False.
        activations (dict[dict[str, torch.Tensor]]): Activations of the modules. Defaults to {}.

    Returns:
        Tuple[torch.Tensor, torch.Tensor]: Returns the transport matrices of the alignment.
    '''
    parser = parameters.get_parser()
    # Changed gpu-id to 0 from -1 to utilize cuda
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    if device.type == 'cuda':
        gpu_id = 0
    else:
        gpu_id = -1
    
    ensemble_type = args["fusion"]["type"]
    
    ot_args = parser.parse_args(['--gpu-id', f'{gpu_id}', '--correction', '--ground-metric', 'euclidean', '--weight-stats', \
                              '--geom-ensemble-type', f'{ensemble_type}', '--ground-metric-normalize', 'none', '--ground-metric-eff', \
                              '--recheck-cifar', '--exact', '--past-correction', '--not-squared', '--return-permutationmatrix', '--ensemble-step', '0'])
    
    
    # amount of heads 
    if ignore_heads:
        number_heads = 1
    else:
        number_heads = module1.head
    hidden = module1.feats 

    #head dimension
    dim_head = hidden // number_heads

    # get qk1, qk2 (concated q and k per head) - these will be fed into wasserstein and aligned
    #  s.t. constraint of softmax is fulfilled (q & k have to be aligned the same way)
    if args["fusion"]["no_qk_stacking"]:
        # get seperate q and k without stacking to study the effect of stacking them
        q1, q2, k1, k2, v1, v2 = split_heads(args, module1, module2, ignore_heads)

        # align every concatenated qk head with wasserstein and save into aligned_qk    
        if not args["fusion"]["type"] == "acts":
            aligned_q_per_head_with_dict = [wasserstein_ensemble.get_wassersteinized_layers_modularized(ot_args, [q1[i], q2[i]]) for i in range(number_heads)]
            aligned_k_per_head_with_dict = [wasserstein_ensemble.get_wassersteinized_layers_modularized(ot_args, [k1[i], k2[i]]) for i in range(number_heads)]
        else:
            # We have to split and concatenate them similarily to the qk1 and qk2 matrices
            q_activations, k_activations, v_activations = split_activations(args, module1, module2, name, activations, ignore_heads=ignore_heads)
            aligned_q_per_head_with_dict = [wasserstein_ensemble.get_acts_wassersteinized_layers_modularized(ot_args, [q1[i], q2[i]], activations=q_activations[i]) for i in range(number_heads)]
            aligned_k_per_head_with_dict = [wasserstein_ensemble.get_acts_wassersteinized_layers_modularized(ot_args, [k1[i], k2[i]], activations=k_activations[i]) for i in range(number_heads)]
            
            q_activations= None
            k_activations = None

        # Concatenate the aligned q and k matrices back into their original shape
        q_in_head_aligned = torch.cat([aligned_q_per_head_with_dict[i][0][0] for i in range(number_heads)], dim=0)
        k_in_head_aligned = torch.cat([aligned_k_per_head_with_dict[i][0][0] for i in range(number_heads)], dim=0)

        # Compute the transport map of the in-head alignment
        for i in range(number_heads):
            T_q_in_head_per_head = aligned_q_per_head_with_dict[i][1]['weight']
            if i == 0:
                T_q_in_head = T_q_in_head_per_head
            else:
                T_q_in_head = torch.block_diag(T_q_in_head, T_q_in_head_per_head)

        for i in range(number_heads):
            T_k_in_head_per_head = aligned_k_per_head_with_dict[i][1]['weight']
            if i == 0:
                T_k_in_head = T_k_in_head_per_head
            else:
                T_k_in_head = torch.block_diag(T_k_in_head, T_k_in_head_per_head)

    else: # qk_stacked
        qk1, qk2, v1, v2 = split_heads(args, module1, module2, ignore_heads)

        # align every concatenated qk head with wasserstein and save into aligned_qk    
        if not args["fusion"]["type"] == "acts":
            aligned_qk_per_head_with_dict = [wasserstein_ensemble.get_wassersteinized_layers_modularized(ot_args, [qk1[i], qk2[i]]) for i in range(number_heads)]
        else:
            qk_activations, v_activations = split_activations(args, module1, module2, name, activations, ignore_heads=ignore_heads)
            #print(f'Aligning qk of layer {name} with activations.')
            aligned_qk_per_head_with_dict = [wasserstein_ensemble.get_acts_wassersteinized_layers_modularized(ot_args, [qk1[i], qk2[i]], activations=qk_activations[i]) for i in range(number_heads)]
            
            qk_activations= None

        # Concatenate the aligned q and k matrices back into their original shape
        q_in_head_aligned = torch.cat([aligned_qk_per_head_with_dict[i][0][0][:,:hidden] for i in range(number_heads)], dim=0)
        k_in_head_aligned = torch.cat([aligned_qk_per_head_with_dict[i][0][0][:,hidden:] for i in range(number_heads)], dim=0)

        # Compute the transport map of the in-head alignment
        for i in range(number_heads):
            T_qk_in_head_per_head = aligned_qk_per_head_with_dict[i][1]['weight']
            if i == 0:
                T_qk_in_head = T_qk_in_head_per_head
            else:
                T_qk_in_head = torch.block_diag(T_qk_in_head, T_qk_in_head_per_head)
    

    if not args['fusion']['type'] == 'acts':
        aligned_v_per_head_with_dict = [wasserstein_ensemble.get_wassersteinized_layers_modularized(ot_args, [v1[i], v2[i]]) for i in range(number_heads)]
    else:
        aligned_v_per_head_with_dict = [wasserstein_ensemble.get_acts_wassersteinized_layers_modularized(ot_args, [v1[i], v2[i]], activations=v_activations[i]) for i in range(number_heads)]
        v_activations = None

    for i in range(number_heads):
        T_v_in_head_per_head = aligned_v_per_head_with_dict[i][1]['weight']
        if i == 0:
            T_v_in_head = T_v_in_head_per_head
        else:
            T_v_in_head = torch.block_diag(T_v_in_head, T_v_in_head_per_head)
    

    v_in_head_aligned = torch.cat([aligned_v_per_head_with_dict[i][0][0][:,:] for i in range(number_heads)], dim=0)

    o_in_head_aligned = module1.o.weight @ T_v_in_head
    

    # save aligned weights
    fused_module.q.weight = nn.Parameter(q_in_head_aligned, requires_grad=False)
    fused_module.k.weight = nn.Parameter(k_in_head_aligned, requires_grad=False)
    fused_module.v.weight = nn.Parameter(v_in_head_aligned, requires_grad=False)
    fused_module.o.weight = nn.Parameter(o_in_head_aligned, requires_grad=False)

    if args['fusion']['no_qk_stacking']:
        return T_q_in_head, T_k_in_head, T_v_in_head
    else:
        return T_qk_in_head, T_v_in_head

def split_activations(args: dict, module1: MultiHeadSelfAttention, module2: MultiHeadSelfAttention, name: str, activations: dict[dict[str, torch.tensor]], ignore_heads: bool = True) -> Tuple[dict[dict[str, torch.tensor]], dict[dict[str, torch.tensor]]]:
    # amount of heads 
    number_heads = module1.head
    hidden = module1.feats
    if ignore_heads:
        number_heads = 1
    #head dimension
    dim_head = hidden // number_heads
    qk_activations = {}
    q_activations = {}
    k_activations = {}
    v_activations = {}
    
    for i in range(number_heads):
        qk_activations[i] = {}
        q_activations[i] = {}
        k_activations[i] = {}
        v_activations[i] = {}
        for idx, _ in enumerate([module1, module2]):
            q_activations[i][idx] = {}
            k_activations[i][idx] = {}
            v_activations[i][idx] = {}
            # Split the q, k and v activations per head
            for act_name, act in activations[idx].items():
                # Make sure to only extract the activations of the current layer
                if name in act_name:
                    if 'q' in act_name:
                        q_activations[i][idx]['weight'] = act[:, :, i*dim_head:(i+1)*dim_head]
                    elif 'k' in act_name:
                        k_activations[i][idx]['weight'] = act[:, :, i*dim_head:(i+1)*dim_head]
                    elif 'v' in act_name:
                        v_activations[i][idx]['weight'] = act[:, :, i*dim_head:(i+1)*dim_head]
            # Concatenate the q and k activations
            if not args['fusion']['no_qk_stacking']:
                qk_activations[i][idx] = {}
                for (_, q_act), (_, k_act) in zip(q_activations[i][idx].items(), k_activations[i][idx].items()):
                    # TODO: Need to find the right name for wassersteinized to work
                    qk_activations[i][idx]['weight'] = torch.cat((q_act, k_act), dim=1)

    if args['fusion']['no_qk_stacking']:
        return q_activations, k_activations, v_activations
    return qk_activations, v_activations



