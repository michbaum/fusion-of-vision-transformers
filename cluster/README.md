## Cluster access
### Setup
To access the cluster you need to first establish a VPN connection. 
Open a shell and use the standart ssh command:
```
ssh username@euler.ethz.ch
```

### get Cluster job via:
When logging in after XYZ time, reload python_gpu 3.9.9 module:
```
module load gcc/8.2.0 python_gpu/3.9.9
ONLY THE FIRST TIME: (from the home directory)
python -m venv --system-site-packages vit
AFTERWARDS JUST ACTIVATE IT (from wherever)
source $HOME/vit/bin/activate
pip install --ignore-installed --no-deps -r requirements.txt
pip install --ignore-installed --no-deps torch==1.12.0+cu113 torchvision==0.13.0+cu113 -f https://download.pytorch.org/whl/cu113/torch_stable.html
```

To queue a job run:
```
sbatch --mem-per-cpu=64G --cpus-per-task=4 --gpus=rtx_3090:1 --time=hh:mm:ss -J "transferlearning"  --wrap="python transfer_learning.py" 
```
Make sure to choose an adequate time for the running job (default is 04:00:00).

To copy some files from your computer to the cluster or vice versa: (-r only needed when copying folders)
```
scp -r /absolute/path/to/folder/ username@euler.ethz.ch:/cluster/home/username/path/to/project          (to cluster)
scp -r username@euler.ethz.ch:/cluster/home/username/path/to/project /absolute/path/to/folder/          (from cluster)
```

### X11
Graphical sessions on Euler are based on X11. This does not provide you with a remote desktop, but allow you to launch graphical applications on the cluster and forward their windows to your local workstation.

The clusters of ID SIS HPC use the X window System (X11) to display a program's graphical user interface (GUI) on a users workstation. You need to install an X11 server on your workstation to siplay X11 windows. The ports used by X11 are blocked by the cluster's firewall. To circumvent this problem, you must open an SSH tunnel and redirect all X11 communication through that tunnel.
Access via:
```
ssh -Y username@euler.ethz.ch
```
Linux:
If you are using a version newer than 1.16 Xorg (X11), then please have a look at the troubleshooting section at the bottom of this wiki page.

Mac OS X:
Since X11 is no longer included in OS X, you must install https://www.xquartz.org/. If you are using a version newer than 2.7.8, then please have a look at the troubleshooting section at the bottom of this wiki page.

Windows:
X11 is not supported by Windows. You can find a list of common X11 servers below:
http://mobaxterm.mobatek.net/
http://x.cygwin.com/
https://sourceforge.net/projects/xming/
https://www.starnet.com/xwin32/

### Add SSH key for git cloning
After accessing cluster:
```
ssh-keygen -t ed25519
```
```
cd ~/.ssh
```
```
cat ~/.ssh/id_ed25519.pub
```
Copy public key and add to git, then git clone.

Presentation on cluster: https://scicomp.ethz.ch/w/images/5/57/Getting_started_with_Euler_%28October_2016%29.pdf


### FAQ
https://scicomp.ethz.ch/wiki/FAQ#How_can_I_become_shareholder.3F


### How do I execute a program on the cluster?
https://scicomp.ethz.ch/wiki/Using_the_batch_system
Every command, program or script that you want to execute on the cluster must be submitted to the bach system (Slurm). The command
```
sbatch
```
is used to submit a batch job and to indicate what resources are required for that job. On Euler, two types of resources must be specified: number of processors and computation time:
```
sbatch --ntasks=#CPUs --time=HH:MM:SS
```

Example:
```
[sfux@eu-login-03 ~]$ sbatch --wrap="gzip big_file.dat"
Submitted batch job 1010113
```
```
[sfux@eu-login-03 ~]$ sbatch --wrap="./hello_world"
Submitted batch job 1010171
```

### LSF/Slurm Submission Advisor
https://scicomp.ethz.ch/public/lsla/index2.html
The LSF/Slurm submission line advisor is a helper tool provided by the HPC group. It facilitates the setup of submission commands and job scripts for the IBM LSF and the Slurm batch system and helps the users to learn the LSF/Slurm syntax.

### Setting up yout environment
https://scicomp.ethz.ch/wiki/Setting_up_your_environment


### Personal Storage
On our clusters, we provide a home directory (folder) for every user that can be used for safe long term storage of important and critical data (program source, script, input file, etc.). It is created on your first login to the cluster and accessible through the path
```
/cluster/home/username
```

### Solution for using VSCode on an HPC Cluster
Visual Studio Code (VSCode) is a popular editor for developers. It has some plugins that allow users to run the editor on their local computer and connect via SSH to a remote system. In the case of an HPC cluster, this is suboptimal, as VSCode will just connect to one of the login nodes and start a large number of threads there. Our system administrators are regularly checking the login nodes and are warning users that are overloading a login node. Users that repeatedly overload login nodes will temporarily be banned from accessing the cluster.
https://gitlab.ethz.ch/sfux/VSCode_remote_HPC
https://scicomp.ethz.ch/wiki/VSCode

### Getting started with GPUs
Not sure whether we can use them "reserved exclusively to the shareholder groups that invested into them".
https://scicomp.ethz.ch/wiki/Getting_started_with_GPUs

### Euler Applications
https://scicomp.ethz.ch/wiki/Euler_applications



