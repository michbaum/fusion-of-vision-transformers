# Fusion of Vision Transformers

Semester Thesis 2023 of Arvid Berg and Simon Schläpfer, based on a Deep Learning Group Project with Micheal Baumgartner

## Workspace setup
### Conda environment

To set up a working environment for development in the project, use the following command from within the project directory

```
conda env create -f environment.yml
```

IF still needed, overwrite the torch and torchvision version with the unstable nightly version for the inference calculations
on the MultiHeadAttention layers. Unfortunately their inputs are not capturable by forward_hooks in the current torch version:
```
pip install torch==2.0.0.dev20221230+cu116 torchvision==0.15.0.dev20221230+cu116 -f https://download.pytorch.org/whl/nightly/torch_nightly.html
```

In case you need to update the requirements of the project, change the environment.yml file accordingly and run
```
conda env update --file environment.yml  --prune
```
from within the active environment.

### Repo structure

```bash
├── README.md                                                       
├── attention_visualization.py        # Python Notebook to investigate/visualize attention
├── cluster                         
│   └── README.md                     # Instructions on using the Euler cluster with this project
├── embedding_extraction.py           # Utilities to transform conv embeddings to linear
├── ensemble.py                       # Utilities to ensemble our parent models for evaluation
├── environment.yml                   # Conda dependency list
├── evaluation.py                     # Utilities to evaluate the fusion methods
├── experiments/                      # Directory with experiment config files
├── fine_tuning.py                    # Utilities to fine-tune (fused) models (currently not implemented)
├── fusion.py                         # Main utilities to fuse two parent ViTs
├── heads_alignment.py                # Utilities to compute the attention between-head alignment
├── in_head_alignment.py              # Utilities to compute the attention in-head alignment
├── main.py                           # Main function to start the fusion
├── network_inspection.py             # Python notebook to investigate structure of ViTs
├── networks/                         # Where pre-trained networks have to be kept
├── networks.py                       # Utilities to load ViTs for our algorithms
├── otfusion_sidak/                   # Utilities from the previous OT Fusion project *1
├── results/                          # Directory where the results are stored
├── requirements.txt                  # Pip requirements
├── split_heads.py                    # Split attention weights into heads
├── train.py                          # Utilities to re-train (fused) models (currently not implemented)
└── vit.py                            # Utilities of the ViT model
```
*1: Some utilities were slightly altered or adapted into heads_alignment.py to suite our needs.

## Run scripts & evaluations

All configurations regarding the models and fusion evaluation can be set in /experiments/default.yaml.  <br />

Model parameters are set in the section "model:"                        # e.g. number of heads. <br />
Fusion parameters are set in the section "fusion:"                      # e.g. acts or wts based. <br />
Evaluation parameters are set in the section "evaluation:"              # e.g. mode connectivity evaluation. <br />
<br />
<br />
<br />
Model parameters such as model seeds, number of heads, number of encoders, mlp- and hidden-width, whether  <br />
there should be residual connections or not etc. can be set in:  <br />
<br />
model:<br />
&nbsp; &nbsp; seeds: ['(11,12)', '(13,14)']    # evaluate fusion between model with seeds 20 and 21, as well as fusion of model with seeds 22 and 23 <br />
&nbsp; &nbsp; num_layers: 7 <br />
&nbsp; &nbsp; num_heads: 12  <br />
&nbsp; &nbsp; hidden: 384 <br />
&nbsp; &nbsp; mlp_hidden: 1536 <br />
&nbsp; &nbsp; resiual: True <br />
&nbsp; &nbsp; ... <br />
<br />
Trained models are in our google drive. All model in the drive are trained with<br />
no Bias and dropout=0.0. For the above example, you should use the models named as below (which correspond<br />
to the specifiations given in the model config above)<br />
<br />
"vit_c10_seed11_aa_ls_nheads_12_hidden_384_mlp_hidden_1536_num_layers_7"<br />
"vit_c10_seed12_aa_ls_nheads_12_hidden_384_mlp_hidden_1536_num_layers_7"<br />
and<br />
"vit_c10_seed13_aa_ls_nheads_12_hidden_384_mlp_hidden_1536_num_layers_7"<br />
"vit_c10_seed14_aa_ls_nheads_12_hidden_384_mlp_hidden_1536_num_layers_7"<br />
<br />
<br />
Generally, there are 3 three different fusion methods called "Vanilla Fusion", "Ignore Heads Fusion" and "Head Aware Fusion".<br />
When evaluating, you can choose any of them or multiple, by adding them into the list of fusion methods in:<br />
<br />
fusion: <br />
&nbsp; &nbsp; methods: ["vanilla"] or ["head_aware"] or [ignore_heads"] or ["head_aware", "ignore_heads"] and so on... <br />
<br />
<br />
Also you can set the type of evaluation you'd like to run (E.g. Mode Connectivity):<br />
evaluation: <br />
&nbsp; &nbsp; evaluation_method: None # if you want to evaluate the specified fusion methods generally <br />
<br />
or <br />
evaluation: <br />
&nbsp; &nbsp; evaluation_method: mode_connectivity # if you want to evaluate the specified fusion's and model's mode connectivity <br />
<br />
<br />
Further description about which parameters can be set are explained in the comments of the default.yaml file.<br />
<br />
<br />
To run and evaluate the fusion:<br />
<br />
&nbsp; &nbsp; python main.py default.yaml



