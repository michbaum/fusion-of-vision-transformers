from torchvision import datasets, transforms

def load_dataset(train_set: bool = True):

    train_dataset = datasets.CIFAR10(
                '../cifar_data/',
                train=train_set,
                download=True,
                transform=transforms.Compose(
                    [transforms.ToTensor(),
                    transforms.Normalize(mean=[0.4914, 0.4822, 0.4465], std=[0.2470, 0.2435, 0.2616])]
                )
    )
    return train_dataset

def train():
    raise NotImplementedError

def main():
    pass

if __name__ == "__main__":
    main()