import torch
from torch import nn
import numpy as np
import time
from os.path import exists
from pathlib import Path
import copy
from copy import deepcopy
from typing import List
import csv
import ast

from train import load_dataset, train
from fusion import fusion, vanilla_fusion
from torch.utils.data import DataLoader
from networks import get_small_vit
from tqdm import tqdm
from ensemble import ensemble
from fine_tuning import fine_tune_network
from vit import ViT
from itertools import permutations, combinations, product


def evaluate_model(args, model: ViT, csv_values: list=[]):
    """Evaluates a given model on the test dataset and writes the result under the given name into the file.

    Args:
        model (ViT): The model to be evaluated.
        name (str): Name of the model.
    """
# seed, num_heads, hidden, mlphidden, num_layers, method
    # Load the test dataset
    # get Path

    if args['evaluation']['evaluation_method'] == 'bruteforce_residuals':
        names = ['seeds_', '_numHeads_', '_hidden_', '_mlpHidden_', '_numLayers_', '_fusionMethod_', '_finetuned_', '_alpha_', '_residualBruteforceCombination_']
    elif args['evaluation']['evaluation_method'] == 'bruteforce_head_alignment':
        names = ['seeds_', '_numHeads_', '_hidden_', '_mlpHidden_', '_numLayers_', '_fusionMethod_', '_finetuned_', '_alpha_', '_headCombination_']
    else:
        names = ['seeds_', '_numHeads_', '_hidden_', '_mlpHidden_', '_numLayers_', '_fusionMethod_', '_finetuned_', '_alpha_', '']

    if args['evaluation']['safe']:
        path = args['evaluation']['safe_dir'] + '_model_'
        for val, name in zip(csv_values, names):
            path += str(name)
            if name == '_alpha_': val = int(val * 100)
            path += str(val)
        torch.save(model.state_dict(), path)
    test_dataset = load_dataset(train_set=False)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    test_loader = DataLoader(test_dataset, batch_size=args["evaluation"]["batch_size"], shuffle=False, num_workers=0)

    # Set the model to evaluation mode
    correct = 0
    total = 0
    test_loss = 0
    test_model = deepcopy(model)
    test_model.to(device)
    test_model.eval()
    criterion = nn.CrossEntropyLoss()
    with torch.no_grad():
        for data in tqdm(test_loader):
            loss = 0
            images, labels = data
            images = images.to(device)
            labels = labels.to(device)
            outputs = test_model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
            loss = criterion(outputs, labels)
            test_loss += loss.item()

    # Compute the test accuracy for the epoch
    test_acc = float(correct / total)
    # is this correct?
    test_loss /= len(test_loader)
    csv_values.append(test_acc)
    csv_values.append(test_loss)

    if args["evaluation"]["train_loss"]:
        train_loss = 0
        train_model = deepcopy(model)
        train_model.to(device)
        train_model.eval()
        train_dataset = load_dataset(train_set=True)
        train_loader = DataLoader(train_dataset, batch_size=args["evaluation"]["batch_size"], shuffle=False, num_workers=0)
        with torch.no_grad():
            for data in tqdm(train_loader):
                loss = 0
                images, labels = data
                images = images.to(device)
                labels = labels.to(device)
                outputs = train_model(images)
                #_, predicted = torch.max(outputs.data, 1)
                #total += labels.size(0)
                #correct += (predicted == labels).sum().item()
                loss = criterion(outputs, labels)
                train_loss += loss.item()
        train_loss /= len(train_loader)
        csv_values.append(train_loss)

    with open(f'results/{args["evaluation"]["csv_name"]}.csv', 'a', newline='') as file:
        writer = csv.writer(file,quoting=csv.QUOTE_NONNUMERIC)
        # Write some data to the CSV file
        writer.writerow(csv_values)
    return test_acc

def evaluate_single_fusion(args: dict, seeds: tuple[int, int] = (10, 11), num_heads: int = 4, fusion_method: str = "head_aware"):
    """Evaluate a single fusion of two models loaded, given the seeds and the number of heads, with a given fusion method on the test set and write results to a file.

    Args:
        fine_tune (bool, optional): Whether to fine-tune the fused model and evaluate it alongside the base fused model. Defaults to False.
        seeds (tuple[int, int], optional): Seeds of the parents to load. Defaults to (10, 11).
        num_heads (int, optional): Number of heads of the parents to load. Defaults to 4.
        fusion_method (str, optional): Fusion method to use. Defaults to "groundtruth".
        hidden (int, optional): Embedding dimension of models. Defaults to 384.
        mlp_hidden (int, optional): Hidden dimension of MLPs in models. Defaults to 4*384.
        residual (bool, optional): Whether to use residual connections in the fusion (affects the transport matrices). Defaults to True.
    """
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    seed1, seed2 = seeds
    hidden = args["model"]["hidden"]
    mlp_hidden = args["model"]["mlp_hidden"]
    num_layers = args["model"]["num_layers"]
    

    if args["model"]["residual"]:
        model1 = get_small_vit(args, num_heads)
        model2 = get_small_vit(args, num_heads)
        model1_name = f"vit_c10_seed{seed1}_aa_ls_nheads_{num_heads}_hidden_{hidden}_mlp_hidden_{mlp_hidden}_num_layers_{num_layers}"
        model2_name = f"vit_c10_seed{seed2}_aa_ls_nheads_{num_heads}_hidden_{hidden}_mlp_hidden_{mlp_hidden}_num_layers_{num_layers}"
        model1.load_state_dict(torch.load(f'networks/{model1_name}.pth', map_location=device))
        model2.load_state_dict(torch.load(f'networks/{model2_name}.pth', map_location=device))
        model1.to(device)
        model2.to(device)

    else:
        model1 = get_small_vit(args, num_heads)
        model2 = get_small_vit(args, num_heads)
        model1_name = f"vit_c10_seed{seed1}_aa_ls_nheads_{num_heads}_no_res_hidden_{hidden}_mlp_hidden_{mlp_hidden}_num_layers_{num_layers}"
        model2_name = f"vit_c10_seed{seed2}_aa_ls_nheads_{num_heads}_no_res_hidden_{hidden}_mlp_hidden_{mlp_hidden}_num_layers_{num_layers}"
        model1.load_state_dict(torch.load(f'networks/{model1_name}.pth', map_location=device))
        model2.load_state_dict(torch.load(f'networks/{model2_name}.pth', map_location=device))
        model1.to(device)
        model2.to(device)
    
    args["fusion"]["method"] = fusion_method
    if args["evaluation"]["evaluation_method"] == "bruteforce_residuals":
        evaluate_residual_bruteforce(args, model1=model1, model2=model2, seed1=seed1, seed2=seed2, num_heads=num_heads)
    elif args["evaluation"]["evaluation_method"] == "mode_connectivity":
        evaluate_mode_connectivity(args, model1=model1, model2=model2, seed1=seed1, seed2=seed2, num_heads=num_heads)
    elif args["evaluation"]["evaluation_method"] == "bruteforce_head_alignment":  
        evaluate_all_combinations(args, model1=model1, model2=model2, seed1=seed1, seed2=seed2, num_heads=num_heads)
    elif fusion_method == "head_aware":
        evaluate_head_aware(args, model1=model1, model2=model2, seed1=seed1, seed2=seed2, num_heads=num_heads)
    elif fusion_method == "vanilla":
        evaluate_vanilla(args, model1=model1, model2=model2, seed1=seed1, seed2=seed2, num_heads=num_heads)
    elif fusion_method == "ensemble":
        evaluate_ensemble(args, model1=model1, model2=model2, name1=model1_name, name2=model2_name, seed1=seed1, seed2=seed2, num_heads=num_heads)
    elif fusion_method == "ignore_heads":
        evaluate_ignore_heads(args, model1=model1, model2=model2, seed1=seed1, seed2=seed2, num_heads=num_heads)
    else:
        raise ValueError(f"Fusion method: {fusion_method} not supported.")


def evaluate_all_fusion(args: dict):
    """Evaluate all model pairs given in seeds and the number of heads with a given fusion method on the test set and write results to a file.

    Args:
        fine_tune (bool, optional): Wheter to also fine-tune the fused models and evaluate them alongside the base fused model. Defaults to False.
        seeds (List[tuple[int, int]], optional): Pairs of seeds corresponding to the parent models to be fused. Defaults to [(10, 11)].
        num_heads (List[int], optional): List of number of heads corresponding to models to be evaluated. Defaults to [4].
        fusion_methods (List[str], optional): List of fusion methods to use. Defaults to ["groundtruth"].
        hidden_mlphidden (List[int], optional): List of tuples of hidden and mlp_hidden sizes to use for the fusion. Defaults to [(384, 4*384)].
        residual (bool, optional): Whether to use residual connections in the fusion (affects the transport matrices). Defaults to True.
    """        
    for fusion_method in args["fusion"]["methods"]:
        for seed in args["model"]["seeds"]:
            curr_seed = ast.literal_eval(seed)
            for num_head in args["model"]["num_heads"]:
                evaluate_single_fusion(args, seeds=curr_seed, num_heads=num_head, fusion_method=fusion_method)

def evaluate_vanilla(args, model1: ViT, model2: ViT, seed1: str, seed2: str, num_heads: int):
    """Evaluates the simple fusion between the two given models and optionally fine tunes the fused model.

    Args:
        model1 (ViT): First parent model.
        model2 (ViT): Second parent model.
        seed1 (str): Seed used in the first parent.
        seed2 (str): Seed used in the second parent.
        num_heads (int, optional): Number of heads in the parents. Defaults to 4.
    """
    hidden = args["model"]["hidden"]
    mlp_hidden = args["model"]["mlp_hidden"]
    num_layers = args["model"]["num_layers"]
    alpha = args["fusion"]["alpha"]
    dc_args = deepcopy(args)

    fused = fusion(dc_args, deepcopy(model1), model2)
    csv_details = [f'{seed1}&{seed2}', num_heads, hidden, mlp_hidden, num_layers, 'dummy', 'no', alpha, ""]
    evaluate_model(dc_args, fused, csv_details)

    if args["evaluation"]["fine_tune"]:
        raise NotImplementedError

def evaluate_ensemble(model1: ViT, model2: ViT, name1: str, name2: str, seed1: str, seed2: str, fine_tune: bool = False, num_heads: int = 4, hidden:int=384, mlp_hidden: int = 1536):
    """Evaluates the ensemble of the two given models and optionally fine tunes the fused model.

    Args:
        model1 (ViT): First parent model.
        model2 (ViT): Second parent model.
        name1 (str): Name of the first parent model.
        name2 (str): Name of the second parent model.
        seed1 (str): Seed used in the first parent.
        seed2 (str): Seed used in the second parent.
        fine_tune (bool, optional): Whether or not to fine tune the fused model and evaluate it alongside the base fusion. Defaults to False.
        num_heads (int, optional): Number of heads in the parents. Defaults to 4.
        hidden (int, optional): Embedding size of the models. Defaults to 384.
        mlp_hidden (int, optional): MLP hidden size of the models. Defaults to 1536.
    """
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    path = f"./evaluations/ensemble_seeds_{seed1}_{seed2}_heads{num_heads}_hidden_{hidden}.txt"
    csv_details = [f'{seed1}&{seed2}', num_heads, hidden, mlp_hidden, 'ensemble', 'no', "", "", ""]
    ensemble([model1, model2], path, csv_details)
    


def evaluate_ignore_heads(args, model1: ViT, model2: ViT, seed1: str, seed2: str, num_heads: int):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    hidden = args["model"]["hidden"]
    mlp_hidden = args["model"]["mlp_hidden"]
    num_layers = args["model"]["num_layers"]
    alpha = args["fusion"]["alpha"]
    dc_args = deepcopy(args)
    path = f"./evaluations/naive_fused_seeds_{seed1}_{seed2}_heads{num_heads}_hidden_{hidden}.txt"
    if args["fusion"]["type"] == "acts":
        fused = fusion(dc_args, deepcopy(model1), model2)
        csv_details = [f'{seed1}&{seed2}', num_heads, hidden, mlp_hidden, num_layers, 'ignore_heads_acts', 'no', alpha, ""]
    else:
        fused = fusion(dc_args, deepcopy(model1), model2)
        csv_details = csv_details = [f'{seed1}&{seed2}', num_heads, hidden, mlp_hidden, num_layers, 'ignore_heads_wts', 'no', alpha, ""]
    evaluate_model(args, fused, csv_details)

    if args["evaluation"]["fine_tune"]:
        raise NotImplementedError


def evaluate_head_aware(args, seed1, seed2, num_heads, model1: ViT, model2: ViT):
    """Evaluates the bootstrap fusion between the two given models and optionally fine tunes the fused model.

    Args:
        model1 (ViT): First parent model.
        model2 (ViT): Second parent model.
        seed1 (str): Seed used in the first parent.
        seed2 (str): Seed used in the second parent.
        fine_tune (bool, optional): Whether or not to fine tune the fused model and evaluate it alongside the base fusion. Defaults to False.
        num_heads (int, optional): Number of heads in the parents. Defaults to 4.
    """
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    hidden = args["model"]["hidden"]
    mlp_hidden = args["model"]["mlp_hidden"]
    num_layers = args["model"]["num_layers"]
    alpha = args["fusion"]["alpha"]
    dc_args = deepcopy(args)

    if args["fusion"]["type"] == "acts":
        fused = fusion(dc_args, deepcopy(model1), model2)
        csv_details = [f'{seed1}&{seed2}', num_heads, hidden, mlp_hidden, num_layers, 'head_aware_acts', 'no',  alpha, ""]
    else:
        fused = fusion(dc_args, deepcopy(model1), model2)
        csv_details = [f'{seed1}&{seed2}', num_heads, hidden, mlp_hidden, num_layers, 'head_aware', 'no', alpha, ""]
    evaluate_model(args, fused, csv_details)

    # This part has to be adapted as it is not up to date
    if args["evaluation"]["fine_tune"]:
        raise NotImplementedError


def evaluate_mode_connectivity(args, model1: ViT, model2: ViT, seed1: str, seed2: str, num_heads: int):
    # only with inference currently
    dc_args = deepcopy(args)
    for alpha in np.arange(0.0, 1.01, 0.1):
        dc_args["fusion"]["alpha"] = alpha
        
        if args["fusion"]["method"] == "vanilla":
            evaluate_vanilla(dc_args, model1=model1, model2=model2, seed1=seed1, seed2=seed2, num_heads=num_heads)
        elif args["fusion"]["method"] == "ignore_heads":
            evaluate_ignore_heads(dc_args, model1=model1, model2=model2, seed1=seed1, seed2=seed2, num_heads=num_heads)
        elif args["fusion"]["method"] == "head_aware":
            evaluate_head_aware(dc_args, model1=model1, model2=model2, seed1=seed1, seed2=seed2, num_heads=num_heads)
        

def evaluate_all_combinations(args, model1: ViT, model2: ViT, seed1: str, seed2: str, num_heads: int):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model1.to(device)
    model2.to(device)
    dc_args = deepcopy(args)
    if num_heads > 4:
        raise ValueError("All head combinations are computationally only feasibly if num_heads <= 4")
    permutation_list = list(permutations(range(num_heads)))
    combinations = [list(comb) for comb in product(permutation_list, repeat=dc_args['model']['num_layers'])]


    dc_args['fusion']['method'] = "all_combinations"
    for combination in combinations:
        dc_args["fusion"]["combination"] = deepcopy(combination)
        fused = fusion(dc_args, deepcopy(model1), deepcopy(model2))
        combination_string_csv = str(combination)
        cleaned_string_csv = combination_string_csv.replace(',', '')

        csv_details = [f'{seed1}&{seed2}', num_heads, dc_args['model']['hidden'], dc_args['model']['mlp_hidden'],  dc_args['model']['num_layers'], f"{dc_args['fusion']['method']}all_head_combs_{args['fusion']['type']}", 'no', dc_args["fusion"]["alpha"], cleaned_string_csv]
        evaluate_model(dc_args, fused, csv_details)


def evaluate_residual_bruteforce(args, model1: ViT, model2: ViT, seed1: str, seed2: str, num_heads: int):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model1.to(device)
    model2.to(device)
    dc_args = deepcopy(args)

    # create all possible resiudal combinations 2^(num_layers*2), for every layer 2 residual connections
    # where 1 stands for using the residual connection and 0 for not using it
    # for example for 2 layers: 0000, 0001, 0010, 0011, 0100, 0101, 0110, 0111
    # create all possible combinations of 0 and 1
    combinations = [list(comb) for comb in product([0, 1], repeat=dc_args['model']['num_layers']*2)]
    if dc_args['model']['num_layers']==7:
        filtered_combinations = [c for c in combinations if sum(c) > 11]
        combinations = filtered_combinations

    for combination in combinations:
        dc_args["fusion"]["residual_bruteforce"] = combination
        fused = fusion(dc_args, deepcopy(model1), deepcopy(model2))
        combination_string_csv = str(combination)
        cleaned_string_csv = combination_string_csv.replace(',', '')
        csv_details = [f'{seed1}&{seed2}', num_heads, dc_args['model']['hidden'], dc_args['model']['mlp_hidden'], dc_args['model']['num_layers'], f"{dc_args['fusion']['method']}_all_residual_combs_{args['fusion']['type']}", 'no', dc_args["fusion"]["alpha"], cleaned_string_csv]

        acc = evaluate_model(dc_args, fused, csv_details)
       


def main():
    pass

    
if __name__ == "__main__":
    main()
