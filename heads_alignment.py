import torch
from torch import nn
from torchvision import models
from otfusion_sidak import parameters
from otfusion_sidak.ground_metric import GroundMetric
import numpy as np
import ot
from argparse import Namespace
from typing import Optional, List
from vit import ViT, MultiHeadSelfAttention
from itertools import permutations
from copy import deepcopy
from split_heads import split_heads
from scipy.optimize import linear_sum_assignment
from otfusion_sidak import wasserstein_ensemble
import torch.nn.functional as F
import matplotlib.pyplot as plt

torch.set_default_dtype(torch.float64)

def align_heads(args: dict, module1: MultiHeadSelfAttention, module2: MultiHeadSelfAttention, fused: ViT, name: str, 
                 activations: dict[dict[str, torch.Tensor]] = {}, alpha: float = 0) -> torch.Tensor:
    """Align heads of two MultiheadAttention modules by computing the optimal transport between all heads.
       Update the weights and biases of the fused model with the averaged weights and biases of the two aligned models.

    Args:
        args (dict): Arguments from the config file.
        module1 (MultiHeadSelfAttention): Module to be aligned.
        module2 (MultiHeadSelfAttention): Anchor module.
        fused (ViT): The fused model.
        name (str): Name of the layer to be aligned.
        activations (dict[dict[str, torch.Tensor]], optional): Dictionary containing the activations of the two models. Defaults to {}.
        alpha (float): Ratio of model to be fused. Defaults to 0, which means they are only aligned and not fused.

    Raises:
        ValueError: If the number of heads of the two MultiheadAttention modules is not equal. This feature is currently not supported.

    Returns:
        torch.Tensor: The transport matrix of the current module.
    """
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    if module1.head != module2.head:
        raise ValueError("The number of heads of the two MultiheadAttention modules is not equal. This feature is currently not supported.")
    num_heads = module1.head
    # Extract the hidden dimension
    hidden_dim = module1.o.weight.shape[1]
    if hidden_dim % num_heads != 0:
        raise ValueError("The number of columns of the weight matrices (head size) is not a multiple of the number of heads.")
    columns_per_head = int(hidden_dim/num_heads)

    # Extract the weights for the queries, key and value matrices W_q, W_k and W_v
    # The matrices get transposed, thus the heads are in columns
    W_q_1 = module1.q.weight.detach().permute(1,0)
    W_q_2 = module2.q.weight.detach().permute(1,0)
    W_k_1 = module1.k.weight.detach().permute(1,0)
    W_k_2 = module2.k.weight.detach().permute(1,0)
    W_v_1 = module1.v.weight.detach().permute(1,0)
    W_v_2 = module2.v.weight.detach().permute(1,0)
    # Here are the heads already in the columns. Thus no transpose is needed
    W_o_1 = module1.o.weight.detach()
    W_o_2 = module2.o.weight.detach()

        
    T = hungarian_assignment(args, module1, module2, activations=activations, name=name)
    T = T.to(device)
        

    # Expand T to match the number of columns per head for a correct permutation
    T_expanded = expand_T(T, columns_per_head)

    # Align query, key, value
    W_q_1_aligned = torch.matmul(W_q_1, T_expanded)
    W_k_1_aligned = torch.matmul(W_k_1, T_expanded)
    W_v_1_aligned = torch.matmul(W_v_1, T_expanded)

    # Align out_proj weights
    W_o_1_aligned = torch.matmul(W_o_1, T_expanded)

    # Average aligned weights and transpose q, k, v back
    W_q_fused = (1 - alpha) * W_q_1_aligned.T + alpha * W_q_2.T
    W_k_fused = (1 - alpha) * W_k_1_aligned.T + alpha * W_k_2.T
    W_v_fused = (1 - alpha) * W_v_1_aligned.T + alpha * W_v_2.T
    W_o_fused = (1 - alpha) * W_o_1_aligned + alpha * W_o_2

    fused.get_submodule(name).q.weight = nn.Parameter(W_q_fused, requires_grad=False)
    fused.get_submodule(name).k.weight = nn.Parameter(W_k_fused, requires_grad=False)
    fused.get_submodule(name).v.weight = nn.Parameter(W_v_fused, requires_grad=False)   
    fused.get_submodule(name).o.weight = nn.Parameter(W_o_fused, requires_grad=False)

    return T_expanded

def hungarian_assignment(args: dict, module1, module2, activations: dict[dict[str, torch.Tensor]] = {}, name = None) -> torch.Tensor:
    """
    For every all heads, calulcate costs between all heads (numheads^2) with groundmetric.process. 
    Then calculate hungarian algorithm assignment and return T.
    Args:
        args: Arguments from the config file.
        module1: First module to align.
        module2: Anchor module
        activations: Dictionary containing the activations of the two models. Defaults to {}.
    Returns:
        torch.tensor: Transport matrix.
    """
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    if device.type == 'cuda':
        gpu_id = 0
    else:
        gpu_id = -1
    parser = parameters.get_parser()

    number_heads = module1.head
    hidden = module1.feats
    columns_per_head = int(hidden/number_heads)
    ensemble_type = 'wts'

    if args["fusion"]["type"] == 'acts':
        ensemble_type = 'acts'
        v_name = name + '.v'
        V_1 = activations[0][v_name] # 1, n * 65, 384
        V_2 = activations[1][v_name]

        V_1_heads = V_1.reshape(V_1.shape[1], number_heads, columns_per_head).permute(1,0,2) # n_heads (4 here), n * 65, emb / n_heads (96 here)
        V_2_heads = V_2.reshape(V_2.shape[1], number_heads, columns_per_head).permute(1,0,2)
        
        q_name = name + '.q'
        Q_1 = activations[0][q_name] # 1, n * 65, 384
        Q_2 = activations[1][q_name]

        Q_1_heads = Q_1.reshape(Q_1.shape[1], number_heads, columns_per_head).permute(1,0,2) # n_heads (4 here), n * 65, emb / n_heads (96 here)
        Q_2_heads = Q_2.reshape(Q_2.shape[1], number_heads, columns_per_head).permute(1,0,2)


        k_name = name + '.k'
        K_1 = activations[0][k_name] # 1, n * 65, 384
        K_2 = activations[1][k_name]

        K_1_heads = K_1.reshape(K_1.shape[1], number_heads, columns_per_head).permute(1,0,2) # n_heads (4 here), n * 65, emb / n_heads (96 here)
        K_2_heads = K_2.reshape(K_2.shape[1], number_heads, columns_per_head).permute(1,0,2)

        # stack Q_1 and K_ 1 and Q_2 and K_2 in dimension 1 with torch.cat
        QK_1 = torch.cat((Q_1_heads, K_1_heads), dim=1) # 4, 2 * n * 65, 96
        QK_2 = torch.cat((Q_2_heads, K_2_heads), dim=1)


        ot_args = parser.parse_args(['--gpu-id', f'{gpu_id}', '--correction', '--ground-metric', 'euclidean', '--weight-stats', \
                            '--geom-ensemble-type', f'{ensemble_type}', '--ground-metric-normalize', 'none', '--ground-metric-eff', \
                            '--recheck-cifar', '--exact', '--past-correction', '--not-squared', '--no-alignment', '--ensemble-step', '0'])
    else:
        ot_args = parser.parse_args(['--gpu-id', f'{gpu_id}', '--correction', '--ground-metric', 'euclidean', '--weight-stats', \
                            '--geom-ensemble-type', f'{ensemble_type}', '--ground-metric-normalize', 'none', '--ground-metric-eff', \
                            '--recheck-cifar', '--exact', '--past-correction', '--not-squared', '--ensemble-step', '0', '--no-alignment'])
    

    # Cost metric used for saving costs between all heads (symmetric)
    M = torch.zeros(number_heads, number_heads)

    if args["fusion"]["single_OT"]:
        args["fusion"]["no_qk_stacking"] = True

    if not args["fusion"]["no_qk_stacking"]:
        qk1, qk2, vo1, vo2 = split_heads(args, module1, module2)
    else: 
        q1, q2, k1, k2, vo1, vo2 = split_heads(args, module1, module2)
    cost_metric_list = []

    if args["fusion"]["type"] == 'acts':
        if args["fusion"]["single_OT"]:
            sqrt_d = module1.feats ** 0.5
            for i in range(number_heads):
                for j in range(number_heads):
                        Q_1_head = Q_1_heads[i].reshape(65, -1, Q_1_heads[i].shape[1]).permute(1,0,2)
                        Q_2_head = Q_2_heads[j].reshape(65, -1, Q_2_heads[i].shape[1]).permute(1,0,2)
                        K_1_head = K_1_heads[i].reshape(65, -1, K_1_heads[i].shape[1]).permute(1,0,2)
                        K_2_head = K_2_heads[j].reshape(65, -1, K_2_heads[i].shape[1]).permute(1,0,2)

                        QK_1_head = F.softmax(torch.einsum("bif, bjf->bij", Q_1_head, K_1_head)/sqrt_d, dim=-1)
                        QK_2_head = F.softmax(torch.einsum("bif, bjf->bij", Q_2_head, K_2_head)/sqrt_d, dim=-1)
                        
                        QK_1_head_flatten = QK_1_head.flatten()
                        QK_2_head_flatten = QK_2_head.flatten()

                        M[i][j] = torch.dist(QK_1_head_flatten, QK_2_head_flatten, p=2)

                        cost_metric_list.append(M[i][j])
        else:
            for i in range(number_heads):
                for j in range(number_heads):
                    if not args["fusion"]["no_qk_stacking"]:
                        qk_dict = {0: {'weight': QK_1[i]}, 1: {'weight': QK_2[j]}}
                        QK = wasserstein_ensemble.get_acts_wassersteinized_layers_modularized(ot_args, [qk1[i], qk2[j]], activations=qk_dict)
                        M_qk = QK[0]
                        T_qk = QK[1]['weight']
                    else:
                        q_dict = {0: {'weight': Q_1_heads[i]}, 1: {'weight': Q_2_heads[j]}}
                        k_dict = {0: {'weight': K_1_heads[i]}, 1: {'weight': K_2_heads[j]}}
                        Q = wasserstein_ensemble.get_acts_wassersteinized_layers_modularized(ot_args, [q1[i], q2[j]], activations=q_dict)
                        K = wasserstein_ensemble.get_acts_wassersteinized_layers_modularized(ot_args, [k1[i], k2[j]], activations=k_dict)
                        M_q = Q[0]
                        M_k = K[0]
                        T_q = Q[1]['weight']
                        T_k = K[1]['weight']
                    v_dict = {0: {'weight': V_1_heads[i]}, 1: {'weight': V_2_heads[j]}}
                    VO = wasserstein_ensemble.get_acts_wassersteinized_layers_modularized(ot_args, [vo1[i], vo2[j]], activations=v_dict)
                    M_vo = VO[0]
                    T_vo = VO[1]['weight']
                    
                    if not args["fusion"]["no_qk_stacking"]:
                        M[i][j] = torch.trace(T_vo.T @ M_vo) + torch.trace(T_qk.T @ M_qk)
                    else:
                        M[i][j] = torch.trace(T_vo.T @ M_vo) + torch.trace(T_q.T @ M_q) + torch.trace(T_k.T @ M_k)
                    
                    
                    cost_metric_list.append(M[i][j])

    else:
        aligned_qk_per_head_with_dict = [wasserstein_ensemble.get_wassersteinized_layers_modularized(ot_args, [qk1[i], qk2[j]]) for i in range(number_heads) for j in range(number_heads)]
        aligned_vo_per_head_with_dict = [wasserstein_ensemble.get_wassersteinized_layers_modularized(ot_args, [vo1[i], vo2[j]]) for i in range(number_heads) for j in range(number_heads)]
        for i in range(number_heads):
            for j in range(number_heads):
                M_qk = aligned_qk_per_head_with_dict[i*number_heads+j][0]['weight']
                T_qk = aligned_qk_per_head_with_dict[i*number_heads+j][1]['weight']
                M_vo = aligned_vo_per_head_with_dict[i*number_heads+j][0]['weight']
                T_vo = aligned_vo_per_head_with_dict[i*number_heads+j][1]['weight']
                M[i][j] = torch.trace(T_qk.T @ M_qk) + torch.trace(T_vo.T @ M_vo)
                cost_metric_list.append(M[i][j])
    

    # apply hungarian assignment
    row_indices, col_indices = linear_sum_assignment(M.cpu().detach().numpy())
    T = torch.zeros(number_heads, number_heads)
    T[row_indices, col_indices] = 1

    # Retrieve the optimal assignments
    return T

def normalize_acts(activations):
    mean_acts = torch.mean(activations, dim=0)
    print("mean shape:", mean_acts.shape)
    std_acts  = torch.std(activations, dim=0)
    return torch.div(torch.sub(activations, mean_acts), torch.add(std_acts, 1e-9))

def expand_T(T: torch.Tensor, columns_per_head: int) -> torch.Tensor:
    """Expand T to match the number of columns of the weight matrices of a head.

    Args:
        T (torch.Tensor): Transport matrix.
        columns_per_head (int): Number of columns of the weight matrices of a head.

    Returns:
        torch.Tensor: Expanded transport matrix.
    """
    device = T.device
    mat1 = T
    mat2 = torch.eye(columns_per_head).to(device)
    kron = torch.kron(mat1, mat2)
    return kron

def get_specific_head_permutation(args: dict, module: MultiHeadSelfAttention, permutation, activations, name) -> MultiHeadSelfAttention:
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    num_heads = module.head
    hidden_dim = module.feats
    columns_per_head = hidden_dim // num_heads
    permuted_module = deepcopy(module)
    permuted_module.q.weight.data = module.q.weight.detach().reshape(num_heads, columns_per_head, hidden_dim)[permutation, :, :].reshape(hidden_dim, hidden_dim)
    permuted_module.k.weight.data = module.k.weight.detach().reshape(num_heads, columns_per_head, hidden_dim)[permutation, :, :].reshape(hidden_dim, hidden_dim)
    permuted_module.v.weight.data = module.v.weight.detach().reshape(num_heads, columns_per_head, hidden_dim)[permutation, :, :].reshape(hidden_dim, hidden_dim)
    permuted_module.o.weight.data = module.o.weight.detach().reshape(hidden_dim, num_heads, columns_per_head)[:, permutation, :].reshape(hidden_dim, hidden_dim)
    if args["fusion"]["type"] == "acts":
        permuted_activations = deepcopy(activations)
        permutation_matrix = torch.eye(module.head)
        permutation_matrix = permutation_matrix[:, permutation]
        T_head = expand_T(permutation_matrix, module.feats // module.head).to(device)
        # permute activations just like the heads
        for layer_name, act in activations[0].items():
                    if name in layer_name and any(att in layer_name for att in ['q', 'k', 'v']):
                        activations[0][layer_name] = activations[0][layer_name] @ T_head
    return permuted_module



def main():
    print(len(list(permutations(range(1)))))

if __name__ == '__main__':
    main()

